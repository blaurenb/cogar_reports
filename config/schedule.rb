# frozen_string_literal: true

# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Learn more: http://github.com/javan/whenever

# The following modifiers can be passed as flags/args when updating the crontabs
# (via `whenever --update-crontab`) like this:
# `whenever --update-crontab --set environment='development'`
# HOWEVER, any settings in this file will be preferred over flags that are
# passed in!

env :PATH, ENV['PATH'] # this is necessary for the server to know where to find the bundle and rails commands.
set :environment, 'development' # the 'prod' instance is a dev env too.
set :output, '/var/www/cogar_reports/log/development.log' # for prod.
# set :output, 'log/development.log' # for dev.

# Define a scheduled task:
every 1.day, at: '3:30 am' do
  rake 'run_kickoff:one_time'
end

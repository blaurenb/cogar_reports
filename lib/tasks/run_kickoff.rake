# frozen_string_literal: true

# to run this, call:
# rake run_kickoff:one_time

namespace :run_kickoff do
  desc 'Test the reports'
  task one_time: :environment do

    KickOffJob.perform_now

    puts "Rake Task Complete"

  end
end

class CreateVoteSummary < ActiveRecord::Migration[6.1]
  def change
    create_table :vote_summaries do |t|
      t.string :url
      t.string :vote_time
      t.string :vote_motion
      t.string :clics_vote_digest
      t.references :hearing_item, foreign_key: true
      t.timestamps
    end
  end
end

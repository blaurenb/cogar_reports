# frozen_string_literal: true

class CreateTargetPages < ActiveRecord::Migration[6.1]
  def change
    create_table :target_pages do |t|
      t.string :url_filepath
      t.string :target_slug
      t.references :parentable, polymorphic: true
      t.timestamps
    end
  end
end

class AddErrDigestToErrorItem < ActiveRecord::Migration[6.1]
  def change
    add_column :error_items, :err_digest, :string
  end
end

# frozen_string_literal: true

class CreateErrorItems < ActiveRecord::Migration[6.1]
  def change
    create_table :error_items do |t|
      t.references :reportable, polymorphic: true
      t.references :target_page, foreign_key: true
      t.integer :sort_error
      t.string :clics_key
      t.text :clics_value
      t.string :target_css
      t.text :target_value

      t.timestamps
    end
  end
end

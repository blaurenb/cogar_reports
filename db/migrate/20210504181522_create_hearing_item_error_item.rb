# frozen_string_literal: true

class CreateHearingItemErrorItem < ActiveRecord::Migration[6.1]
  def change
    create_table :hearing_item_error_items do |t|
      t.references :hearing_item, foreign_key: true
      t.references :error_item, foreign_key: true
      t.timestamps
    end
  end
end

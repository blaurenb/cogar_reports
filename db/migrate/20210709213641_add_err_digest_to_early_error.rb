class AddErrDigestToEarlyError < ActiveRecord::Migration[6.1]
  def change
    add_column :early_errors, :err_digest, :string
  end
end

class CreateVoteSummaryErrorItem < ActiveRecord::Migration[6.1]
  def change
    create_table :vote_summary_error_items do |t|
      t.references :vote_summary, foreign_key: true
      t.references :error_item, foreign_key: true
      t.timestamps
    end
  end
end

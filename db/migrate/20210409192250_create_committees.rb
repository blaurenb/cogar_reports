# frozen_string_literal: true

class CreateCommittees < ActiveRecord::Migration[6.1]
  def change
    create_table :committees do |t|
      t.string :clics_committee_id
      t.string :clics_lm_digest
      t.string :meeting_dates, array: true, default: []
      t.string :meeting_uids, array: true, default: []
      t.timestamps
    end
  end
end

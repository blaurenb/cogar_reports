class AddVotesToHearingItem < ActiveRecord::Migration[6.1]
  def change
    add_column :hearing_items, :votes, :jsonb, default: {}
  end
end

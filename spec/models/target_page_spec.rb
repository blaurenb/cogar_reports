# frozen_string_literal: true

# == Schema Information
#
# Table name: target_pages
#
#  id              :bigint           not null, primary key
#  parentable_type :string
#  target_slug     :string
#  url_filepath    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parentable_id   :bigint
#
# Indexes
#
#  index_target_pages_on_parentable  (parentable_type,parentable_id)
#
require 'rails_helper'

RSpec.describe TargetPage, type: :model do
  context 'Validations' do
    let!(:report) { Reports::CommitteeReport.create }
    let!(:committee) do
      Committee.create!(clics_lm_digest: 'somestringthatisahash',
                        meeting_dates: %w[date1 date2],
                        meeting_uids: %w[mtg1 mtg2],
                        clics_committee_id: 'somestringfromclics')
    end

    it 'is invalid without all validated fields' do
      expect(TargetPage
              .new(
                parentable_type: 'Committee',
                target_slug: 'target_slug',
                url_filepath: 'url_filepath'
              )).to_not be_valid
      expect(TargetPage
              .new(
                parentable_id: committee.id,
                target_slug: 'target_slug',
                url_filepath: 'url_filepath'
              )).to_not be_valid
      expect(TargetPage
              .new(
                parentable_id: committee.id,
                parentable_type: 'Committee',
                url_filepath: 'url_filepath'
              )).to_not be_valid
      expect(TargetPage
              .new(
                parentable_id: committee.id,
                parentable_type: 'Committee',
                target_slug: 'target_slug'
              )).to_not be_valid
    end

    it 'is valid with all 5 validated fields' do
      expect(TargetPage
              .new(parentable_id: committee.id,
                   parentable_type: 'Committee',
                   target_slug: 'target_slug',
                   url_filepath: 'url_filepath')).to be_valid
    end
  end

  context 'Class Methods' do
    it 'self.interim_or_yearround(value)' do
      result = TargetPage.interim_or_yearround('')
      expect(result).to eq(nil)

      result = TargetPage.interim_or_yearround(nil)
      expect(result).to eq(nil)

      result = TargetPage.interim_or_yearround(1)
      expect(result).to eq('other')

      result = TargetPage.interim_or_yearround('Some String')
      expect(result).to eq('other')

      result = TargetPage.interim_or_yearround('House')
      expect(result).to eq('house')

      result = TargetPage.interim_or_yearround('Senate')
      expect(result).to eq('senate')

      result = TargetPage.interim_or_yearround('Interim Committee')
      expect(result).to eq('interim')

      result = TargetPage.interim_or_yearround('Joint Committee')
      expect(result).to eq('year-round')
    end
  end

  context 'Methods' do
  end
end

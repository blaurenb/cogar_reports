# frozen_string_literal: true

# == Schema Information
#
# Table name: reports
#
#  id                :bigint           not null, primary key
#  resourceable_type :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  resourceable_id   :bigint
#
# Indexes
#
#  index_reports_on_resourceable  (resourceable_type,resourceable_id)
#
require 'rails_helper'

RSpec.describe Reports::Report, type: :model do
  context 'Report type: CommitteeReport' do
    it 'creates a valid CommitteeReport' do
      report = Reports::CommitteeReport.create!
      expect(report.type).to eq('Reports::CommitteeReport')
    end
  end

  context 'Methods' do
  end
end

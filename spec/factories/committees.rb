# frozen_string_literal: true

# == Schema Information
#
# Table name: committees
#
#  id                 :bigint           not null, primary key
#  abbreviation       :string
#  clics_lm_digest    :string
#  meeting_dates      :string           default([]), is an Array
#  meeting_uids       :string           default([]), is an Array
#  session_year       :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  clics_committee_id :string
#
FactoryBot.define do
  factory :committee do
    id { 1 }

    # Add additional fields as required via your User model
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User visits Show page', type: :feature do
  let!(:report) { Reports::CommitteeReport.create! }

  let!(:c1) do
    Committee.create!(clics_lm_digest: 'digest for MY_COMM_1_2021A', clics_committee_id: 'H__COMM_1_2021A')
  end
  let!(:t1) do
    TargetPage.create!(parentable_id: c1.id,
                       parentable_type: 'Committee',
                       target_slug: 'my_committee_1',
                       url_filepath: '/committees/house/2021a/my_committee_1')
  end
  let!(:c1_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_1_2021A_1234', meeting_uid: 'H_1_2021A_1234') }
  let!(:ch1) { CommitteeHearing.create!(committee_id: c1.id, hearing_id: c1_h1.id) }
  let!(:c1_h1_hi1) { HearingItem.create!(hearing_id: c1_h1.id, hearing_item_uuid: 'H_1_2021A_1234_AAA') }
  let!(:ee_c1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      sort_error: 0)
  end
  let!(:ee_c1_h1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      sort_error: 0)
  end
  let!(:ee_c1_h1_hi1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      sort_error: 10)
  end
  let!(:cee_c1) { CommitteeErrorItem.create!(error_item_id: ee_c1.id, committee_id: c1.id) }
  let!(:hee_c1_h1) { HearingErrorItem.create!(error_item_id: ee_c1_h1.id, hearing_id: c1_h1.id) }
  let!(:hiee_c1_h1_hi1) do
    HearingItemErrorItem.create!(error_item_id: ee_c1_h1_hi1.id, hearing_item_id: c1_h1_hi1.id)
  end

  # Committee 2, all errors are 5
  let!(:c2) do
    Committee.create!(clics_lm_digest: 'digest for MY_COMM_2_2022A', clics_committee_id: 'S_COMM_2_2022A')
  end
  let!(:t2) do
    TargetPage.create!(parentable_id: c2.id,
                       parentable_type: 'Committee',
                       target_slug: 'my_committee_1',
                       url_filepath: '/committees/house/2021a/my_committee_1')
  end
  let!(:c2_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_2_2022A_1234', meeting_uid: 'H_2_2022A_1234') }
  let!(:ch2) { CommitteeHearing.create!(committee_id: c2.id, hearing_id: c2_h1.id) }
  let!(:c2_h1_hi1) { HearingItem.create!(hearing_id: c2_h1.id, hearing_item_uuid: 'H_2_2022A_1234_AAA') }
  let!(:ee_c2) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t2.id,
                      sort_error: 4)
  end
  let!(:ee_c2_h1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t2.id,
                      sort_error: 5)
  end
  let!(:ee_c2_h1_hi1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t2.id,
                      sort_error: 6)
  end
  let!(:cee_c2) { CommitteeErrorItem.create!(error_item_id: ee_c2.id, committee_id: c2.id) }
  let!(:hee_c2_h1) { HearingErrorItem.create!(error_item_id: ee_c2_h1.id, hearing_id: c2_h1.id) }
  let!(:hiee_c2) { HearingItemErrorItem.create!(error_item_id: ee_c2_h1_hi1.id, hearing_item_id: c2_h1_hi1.id) }

  # Committee 3, all errors are 8
  let!(:c3) do
    Committee.create!(clics_lm_digest: 'digest for MY_COMM_3_2023A', clics_committee_id: 'J_COMM_3_2023A')
  end
  let!(:t3) do
    TargetPage.create!(parentable_id: c3.id,
                       parentable_type: 'Committee',
                       target_slug: 'my_committee_1',
                       url_filepath: '/committees/house/2021a/my_committee_1')
  end
  let!(:c3_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_3_2023A_1234', meeting_uid: 'H_3_2023A_1234') }
  let!(:ch3) { CommitteeHearing.create!(committee_id: c3.id, hearing_id: c3_h1.id) }
  let!(:c3_h1_hi1) { HearingItem.create!(hearing_id: c3_h1.id, hearing_item_uuid: 'H_3_2023A_1234_AAA') }
  let!(:ee_c3) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t3.id,
                      sort_error: 7)
  end
  let!(:ee_c3_h1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t3.id,
                      sort_error: 8)
  end
  let!(:ee_c3_h1_hi1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t3.id,
                      sort_error: 9)
  end
  let!(:cee_c3) { CommitteeErrorItem.create!(error_item_id: ee_c3.id, committee_id: c3.id) }
  let!(:hee_c3_h1) { HearingErrorItem.create!(error_item_id: ee_c3_h1.id, hearing_id: c3_h1.id) }
  let!(:hiee_c3) { HearingItemErrorItem.create!(error_item_id: ee_c3_h1_hi1.id, hearing_item_id: c3_h1_hi1.id) }

  it 'Sees elements on the page' do
    visit report_path(report.id)
    # save_and_open_page
    expect(page).to have_content('All Reports')
    expect(page).to have_content('Most Recent Report')
    expect(page).to have_content('Loading')

    expect(page).to_not have_content("Early Errors (we hit a snag and couldn't test the Webpages)")
    expect(page).to_not have_content('TargetPage')
  end

  it 'lazy-loads the stats section', js: true do
    # The initial pageload will find that it is loading the stats section (SEE
    # ABOVE), then when we ask capybara to check for id's that are part of a
    # lazy-loaded sction, it will wait up to 2 whole seconds to see if they show
    # up.

    visit report_path(Reports::CommitteeReport.first.id)

    within '#stats-tic' do
      expect(page).to have_content('Total Items checked')
      expect(page).to have_content('9')
    end
    within '#stats-tee' do
      expect(page).to have_content('Total Early Errors')
      expect(page).to have_content('0')
    end
    within '#stats-tce' do
      expect(page).to have_content('Total Comparison Errors')
      expect(page).to have_content('7')
    end
    within '#stats-ce' do
      expect(page).to have_content('Comparison Errors (not including "clics and coga empty values")')
      expect(page).to have_content('6')
    end
    within '#stats-te1' do
      expect(page).to have_content('system: error saving object')
      expect(page).to have_content('0')
    end
    within '#stats-te2' do
      expect(page).to have_content('clics: json error ')
      expect(page).to have_content('0')
    end
    within '#stats-te3' do
      expect(page).to have_content('coga api: json error')
      expect(page).to have_content('0')
    end
    within '#stats-te4' do
      expect(page).to have_content('clics api: json empty')
      expect(page).to have_content('1')
    end
    within '#stats-te5' do
      expect(page).to have_content('clics and coga empty values')
      expect(page).to have_content('1')
    end
    within '#stats-te6' do
      expect(page).to have_content('clics empty value')
      expect(page).to have_content('1')
    end
    within '#stats-te7' do
      expect(page).to have_content('coga empty value')
      expect(page).to have_content('1')
    end
    within '#stats-te8' do
      expect(page).to have_content('coga mismatched value')
      expect(page).to have_content('1')
    end
    within '#stats-te9' do
      expect(page).to have_content('coga incorrect order or missing values')
      expect(page).to have_content('1')
    end
    within '#stats-te10' do
      expect(page).to have_content('coga page: application error')
      expect(page).to have_content('1')
    end
  end
end

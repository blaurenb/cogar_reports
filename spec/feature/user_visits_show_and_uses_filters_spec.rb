# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User selects filters on the Show Page', type: :feature, js: true do
  let!(:report) { Reports::CommitteeReport.create! }
  let!(:report2) { Reports::CommitteeReport.create! }
  let!(:throwaway) { report2.update(created_at: report.created_at - 1.days) }

  let!(:c1) do
    Committee.create!(clics_lm_digest: 'digest for MY_COMM1_2021A', clics_committee_id: 'H_COMM1_2021A')
  end
  let!(:t1) do
    TargetPage.create!(parentable_id: c1.id,
                       parentable_type: 'Committee',
                       target_slug: 'my_committee_1',
                       url_filepath: "some-url-here/#{c1.id}")
  end
  let!(:ee1_r1) do
    EarlyError.create!(reportable_id: report.id,
                       reportable_type: report.type,
                       description: 'something bad for H_COMM1_2021A (ee1_r1)',
                       err_digest: 'shared_early_error_1_for_report_1',
                       sort_error: 1)
  end
  let!(:ei1_r1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      err_digest: 'shared_error_item_1_for_report_1',
                      sort_error: 5)
  end
  let!(:ee2_r1) do
    EarlyError.create!(reportable_id: report.id,
                       reportable_type: report.type,
                       description: 'something bad for H_COMM1_2021A (ee2_r1)',
                       err_digest: 'unique_early_error_2_for_report_1',
                       sort_error: 1)
  end
  let!(:ei2_r1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      err_digest: 'unique_error_item_2_for_report_1',
                      sort_error: 5)
  end
  let!(:ee1_r2) do
    EarlyError.create!(reportable_id: report2.id,
                       reportable_type: report2.type,
                       description: 'something bad for H_COMM1_2021A (ee1_r2)',
                       err_digest: 'shared_early_error_1_for_report_1',
                       sort_error: 1)
  end
  let!(:ei1_r2) do
    ErrorItem.create!(reportable_id: report2.id,
                      reportable_type: report2.type,
                      target_page_id: t1.id,
                      err_digest: 'shared_error_item_1_for_report_1',
                      sort_error: 5)
  end
  let!(:c1_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_1_2021A_1234', meeting_uid: 'H_1_2021A_1234') }
  let!(:ch1) { CommitteeHearing.create!(committee_id: c1.id, hearing_id: c1_h1.id) }
  let!(:c1_h1_hi1) { HearingItem.create!(hearing_id: c1_h1.id, hearing_item_uuid: 'H_1_2021A_1234_AAA') }
  let!(:ee_c1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      sort_error: 0)
  end
  let!(:ee_c1_h1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      sort_error: 0)
  end
  let!(:ee_c1_h1_hi1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t1.id,
                      sort_error: 10)
  end
  let!(:cee_c1) { CommitteeErrorItem.create!(error_item_id: ee_c1.id, committee_id: c1.id) }
  let!(:hee_c1_h1) { HearingErrorItem.create!(error_item_id: ee_c1_h1.id, hearing_id: c1_h1.id) }
  let!(:hiee_c1_h1_hi1) do
    HearingItemErrorItem.create!(error_item_id: ee_c1_h1_hi1.id, hearing_item_id: c1_h1_hi1.id)
  end

  # Committee 2
  let!(:c2) do
    Committee.create!(clics_lm_digest: 'digest for MY_COMM2_2016A', clics_committee_id: 'S_COMM2_2016A')
  end
  let!(:t2) do
    TargetPage.create!(parentable_id: c2.id,
                       parentable_type: 'Committee',
                       target_slug: 'my_committee_1',
                       url_filepath: "some-url-here/#{c2.id}")
  end
  let!(:c2_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_2_2016A_1234', meeting_uid: 'H_2_2016A_1234') }
  let!(:ch2) { CommitteeHearing.create!(committee_id: c2.id, hearing_id: c2_h1.id) }
  let!(:c2_h1_hi1) { HearingItem.create!(hearing_id: c2_h1.id, hearing_item_uuid: 'H_2_2016A_1234_AAA') }
  let!(:ee_c2) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t2.id,
                      sort_error: 4)
  end
  let!(:ee_c2_h1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t2.id,
                      sort_error: 5)
  end
  let!(:ee_c2_h1_hi1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t2.id,
                      sort_error: 6)
  end
  let!(:cee_c2) { CommitteeErrorItem.create!(error_item_id: ee_c2.id, committee_id: c2.id) }
  let!(:hee_c2_h1) { HearingErrorItem.create!(error_item_id: ee_c2_h1.id, hearing_id: c2_h1.id) }
  let!(:hiee_c2) { HearingItemErrorItem.create!(error_item_id: ee_c2_h1_hi1.id, hearing_item_id: c2_h1_hi1.id) }

  # Committee 3
  let!(:c3) do
    Committee.create!(clics_lm_digest: 'digest for MY_COMM3_2019A', clics_committee_id: 'J_COMM3_2019A')
  end
  let!(:t3) do
    TargetPage.create!(parentable_id: c3.id,
                       parentable_type: 'Committee',
                       target_slug: 'my_committee_1',
                       url_filepath: "some-url-here/#{c3.id}")
  end
  let!(:c3_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_3_2019A_1234', meeting_uid: 'H_3_2019A_1234') }
  let!(:ch3) { CommitteeHearing.create!(committee_id: c3.id, hearing_id: c3_h1.id) }
  let!(:c3_h1_hi1) { HearingItem.create!(hearing_id: c3_h1.id, hearing_item_uuid: 'H_3_2019A_1234_AAA') }
  let!(:ee_c3) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t3.id,
                      sort_error: 7)
  end
  let!(:ee_c3_h1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t3.id,
                      sort_error: 8)
  end
  let!(:ee_c3_h1_hi1) do
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: t3.id,
                      sort_error: 9)
  end
  let!(:cee_c3) { CommitteeErrorItem.create!(error_item_id: ee_c3.id, committee_id: c3.id) }
  let!(:hee_c3_h1) { HearingErrorItem.create!(error_item_id: ee_c3_h1.id, hearing_id: c3_h1.id) }
  let!(:hiee_c3) { HearingItemErrorItem.create!(error_item_id: ee_c3_h1_hi1.id, hearing_item_id: c3_h1_hi1.id) }

  context 'making selections without clicking Filter button' do
    it 'Sees all dropdown options on pageload' do
      visit report_path(report.id)
      # save_and_open_page

      within '#early_sort_errors' do
        options = ['None', 'All', 'system: error saving object', 'clics: json error', 'coga api: json error']
        options.each { |o| expect(page).to have_content(o) }
      end

      within '#comparison_sort_errors' do
        options = ['None', 'ok', 'coga page: application error', 'clics api: json empty', 'clics and coga empty values',
                   'clics empty value', 'coga empty value', 'coga mismatched value', 'coga incorrect order or missing values']
        options.each { |o| expect(page).to have_content(o) }
      end

      within '#sessions' do
        options = %w[All 2021a 2020b 2020a 2019a 2018a 2017b 2017a 2016a]
        options.each { |o| expect(page).to have_content(o) }
      end

      within '#committee_origins' do
        options = %w[All House Senate Conference Joint]
        options.each { |o| expect(page).to have_content(o) }
      end

      within '#committee_ids' do
        options = %w[All H_COMM1_2021A]
        options.each { |o| expect(page).to have_content(o) }
      end
    end

    it 'clicks on sessions to change the committee id options', js: true do
      visit report_path(report.id)

      unselect('2021a', from: 'sessions')
      select('2016a', from: 'sessions')

      within '#committee_ids' do
        options = %w[H_COMM1_2021A J_COMM3_2019A]
        options.each { |o| expect(page).to_not have_content(o) }
        expect(page).to have_content('S_COMM2_2016A')
      end

      select('2019a', from: 'sessions')
      within '#committee_ids' do
        expect(page).to_not have_content('H_COMM1_2021A')
        expect(page).to have_content('S_COMM2_2016A')
        expect(page).to have_content('J_COMM3_2019A')
      end

      unselect('2019a', from: 'sessions')
      unselect('2016a', from: 'sessions')
      select('All', from: 'sessions')
      within '#committee_ids' do
        options = %w[All H_COMM1_2021A J_COMM3_2019A S_COMM2_2016A]
        options.each { |o| expect(page).to have_content(o) }
      end
    end

    it 'clicks on committee origin to change the committee id options', js: true do
      visit report_path(report.id)

      unselect('2021a', from: 'sessions')
      select('All', from: 'sessions')

      unselect('All', from: 'sessions')
      select('House', from: 'committee_origins')
      within '#committee_ids' do
        options = %w[J_COMM3_2019A S_COMM2_2016A]
        options.each { |o| expect(page).to_not have_content(o) }
        expect(page).to have_content('H_COMM1_2021A')
      end

      select('Senate', from: 'committee_origins')
      within '#committee_ids' do
        expect(page).to_not have_content('H_COMM1_2J_COMM3_2019A021A')
        expect(page).to have_content('H_COMM1_2021A')
        expect(page).to have_content('S_COMM2_2016A')
      end

      select('Joint', from: 'committee_origins')
      within '#committee_ids' do
        expect(page).to have_content('H_COMM1_2021A')
        expect(page).to have_content('S_COMM2_2016A')
        expect(page).to have_content('J_COMM3_2019A')
      end

      unselect('All', from: 'committee_origins')
      unselect('House', from: 'committee_origins')
      unselect('Senate', from: 'committee_origins')
      unselect('Joint', from: 'committee_origins')
      select('Conference', from: 'committee_origins')
      within '#committee_ids' do
        options = %w[J_COMM3_2019A S_COMM2_2016A H_COMM1_2021A]
        options.each { |o| expect(page).to_not have_content(o) }
      end

      unselect('Conference', from: 'committee_origins')
      select('All', from: 'committee_origins')
      within '#committee_ids' do
        options = %w[All J_COMM3_2019A S_COMM2_2016A H_COMM1_2021A]
        options.each { |o| expect(page).to have_content(o) }
      end
    end
  end

  context 'making selections, clicking the Filter button, waiting for lazy-loaded sections', js: true do
    it 'Loads Stats, EarlyErrors and ErrorItems from Daily Diff section' do
      visit report_path(report.id)

      within '#filter-1-day' do
        click_on 'Filter'
      end

      # All but 2 of the errors created for this test are for 'today'. There are 1 EE and 1 EI that were created 'yesterday' which means all extant errors for 'today' should show up.
      within '#stats-outer-section' do
        expect(page).to have_content('Stats { Current filters: New issues since Yesterday }')
      end

      within('#stats-tic') { expect(page).to have_content('7') }
      within('#stats-tee') { expect(page).to have_content('1') }
      within('#stats-tce') { expect(page).to have_content('6') }
      within('#stats-ce') { expect(page).to have_content('6') }
      within('#stats-te1') { expect(page).to have_content('1') }
      within('#stats-te2') { expect(page).to have_content('0') }
      within('#stats-te3') { expect(page).to have_content('0') }
      within('#stats-te4') { expect(page).to have_content('1') }
      within('#stats-te5') { expect(page).to have_content('0') }
      within('#stats-te6') { expect(page).to have_content('1') }
      within('#stats-te7') { expect(page).to have_content('1') }
      within('#stats-te8') { expect(page).to have_content('1') }
      within('#stats-te9') { expect(page).to have_content('1') }
      within('#stats-te10') { expect(page).to have_content('1') }

      within '#filtered-early-errors' do
        expect(page).to have_content("Early Errors (we hit a snag and couldn't test the Webpages)")
        expect(page).to have_content('Sortable Error')
        expect(page).to have_content('Error Explanation')

        within "#ee-id-#{ee2_r1.id}" do
          expect(page).to have_content('system: error saving object')
          expect(page).to have_content('something bad for H_COMM1_2021A (ee2_r1)')
        end
      end

      within '#filtered-error-items' do
        # save_and_open_page
        expect(page).to have_content('Comparison Errors')
        within "##{c1.clics_committee_id}" do
          expect(page).not_to have_content(ei1_r1.sort_error)
          expect(page).to have_content(ee_c1_h1_hi1.sort_error)
        end
        within "##{c2.clics_committee_id}" do
          expect(page).to have_content(c2.clics_committee_id, count: 2)
          expect(page).to have_content(ee_c2.sort_error)
          expect(page).to_not have_content(ee_c2_h1.sort_error)
          expect(page).to have_content(ee_c2_h1.hearing_error_item.hearing.meeting_uid, count: 2)
          expect(page).to have_content(ee_c2_h1_hi1.sort_error)
          expect(page).to have_content(ee_c2_h1_hi1.hearing_item_error_item.hearing_item.hearing_item_uuid, count: 1)
        end
        within "##{c3.clics_committee_id}" do
          expect(page).to have_content(c3.clics_committee_id, count: 3)
          expect(page).to have_content(ee_c3.sort_error)
          expect(page).to have_content(ee_c3_h1.sort_error)
          expect(page).to have_content(ee_c3_h1.hearing_error_item.hearing.meeting_uid, count: 3)
          expect(page).to have_content(ee_c3_h1_hi1.sort_error)
          expect(page).to have_content(ee_c3_h1_hi1.hearing_item_error_item.hearing_item.hearing_item_uuid, count: 1)
        end
      end
    end

    context 'Loads Stats, EarlyErrors and ErrorItems from multi-selection section' do
      it 'with all EE AND EI for one session, all committees' do
        c3.update(clics_lm_digest: 'digest for MY_COMM3_2021A', clics_committee_id: 'J_COMM3_2021A')
        t3.update(url_filepath: "some-url-here/#{c3.clics_committee_id}")
        c3_h1.update(clics_m_id_digest: 'digest for H_3_2021A_1234', meeting_uid: 'H_3_2021A_1234')
        c3_h1_hi1.update(hearing_item_uuid: 'H_3_2021A_1234_AAA')

        visit report_path(report.id)

        unselect('None', from: 'early_sort_errors')
        select('All', from: 'early_sort_errors')
        unselect('None', from: 'comparison_sort_errors')
        select('coga empty value', from: 'comparison_sort_errors')
        select('coga mismatched value', from: 'comparison_sort_errors')
        select('coga incorrect order or missing values', from: 'comparison_sort_errors')
        select('coga page: application error', from: 'comparison_sort_errors')

        within '#multi-selection-section' do
          click_on 'Filter'
        end

        expect(page).to  have_select('early_sort_errors', selected: 'All')
        expect(page).to  have_select('comparison_sort_errors',
                                     selected: ['coga page: application error', 'coga empty value', 'coga mismatched value',
                                                'coga incorrect order or missing values'])

        within('#stats-tic') { expect(page).to have_content('6') }
        # save_and_open_page
        within('#stats-tee') { expect(page).to have_content('2') }
        within('#stats-tce') { expect(page).to have_content('4') }
        within('#stats-ce') { expect(page).to have_content('4') }
        within('#stats-te1') { expect(page).to have_content('2') }
        within('#stats-te2') { expect(page).to have_content('0') }
        within('#stats-te3') { expect(page).to have_content('0') }
        within('#stats-te4') { expect(page).to have_content('0') }
        within('#stats-te5') { expect(page).to have_content('0') }
        within('#stats-te6') { expect(page).to have_content('0') }
        within('#stats-te7') { expect(page).to have_content('1') }
        within('#stats-te8') { expect(page).to have_content('1') }
        within('#stats-te9') { expect(page).to have_content('1') }
        within('#stats-te10') { expect(page).to have_content('1') }

        within '#stats-outer-section' do
          expect(page).to have_content('Stats { Current filters: Early Errors: All | Comparison Errors: coga page: application error, coga empty value, coga mismatched value, coga incorrect order or missing values | Sessions: 2021a | Comm Origins: All | Committee Ids: All }')
        end

        within '#filtered-early-errors' do
          expect(page).to have_content("Early Errors (we hit a snag and couldn't test the Webpages)")

          within "#ee-id-#{ee1_r1.id}" do
            expect(page).to have_content('system: error saving object')
            expect(page).to have_content('something bad for H_COMM1_2021A (ee1_r1)')
          end

          within "#ee-id-#{ee2_r1.id}" do
            expect(page).to have_content('system: error saving object')
            expect(page).to have_content('something bad for H_COMM1_2021A (ee2_r1)')
          end
        end

        within '#filtered-error-items' do
          expect(page).to have_content('Comparison Errors')

          within "##{c1.clics_committee_id}" do
            expect(page).to_not have_content(ee_c1.sort_error)
            expect(page).to have_content(ee_c1_h1_hi1.sort_error)
            expect(page).to_not have_content(ee_c1_h1.sort_error)

            expect(page).to have_content(c1.clics_committee_id, count: 1)
            expect(page).to have_content(ee_c1_h1.hearing_error_item.hearing.meeting_uid, count: 2)
            expect(page).to have_content(ee_c1_h1_hi1.hearing_item_error_item.hearing_item.hearing_item_uuid, count: 1)
          end

          within "##{c3.clics_committee_id}" do
            expect(page).to have_content(ee_c3.sort_error)
            expect(page).to have_content(ee_c3_h1.sort_error)
            expect(page).to have_content(ee_c3_h1_hi1.sort_error)

            expect(page).to have_content(c3.clics_committee_id, count: 4)
            expect(page).to have_content(ee_c3_h1.hearing_error_item.hearing.meeting_uid, count: 3)
            expect(page).to have_content(ee_c3_h1_hi1.hearing_item_error_item.hearing_item.hearing_item_uuid, count: 1)
          end
        end
      end

      it 'with all/some EE AND EI for all sessions, one committee_type' do
        visit report_path(report.id)

        unselect('None', from: 'early_sort_errors')
        select('All', from: 'early_sort_errors')
        unselect('None', from: 'comparison_sort_errors')
        select('clics api: json empty', from: 'comparison_sort_errors')
        select('clics and coga empty values', from: 'comparison_sort_errors')
        select('clics empty value', from: 'comparison_sort_errors')
        select('coga empty value', from: 'comparison_sort_errors')
        select('coga mismatched value', from: 'comparison_sort_errors')
        select('coga incorrect order or missing values', from: 'comparison_sort_errors')
        select('coga page: application error', from: 'comparison_sort_errors')
        unselect('2021a', from: 'sessions')
        unselect('All', from: 'committee_origins')
        select('Joint', from: 'committee_origins')
        # waste_seconds(3)

        within '#multi-selection-section' do
          click_on 'Filter'
        end

        expect(page).to  have_select('early_sort_errors', selected: 'All')
        expect(page).to  have_select('comparison_sort_errors',
                                     selected: ['coga page: application error', 'clics api: json empty', 'clics and coga empty values', 'clics empty value',
                                                'coga empty value', 'coga mismatched value', 'coga incorrect order or missing values'])
        expect(page).to  have_select('committee_origins', selected: 'Joint')

        within('#stats-tic') { expect(page).to have_content('3') }
        within('#stats-tee') { expect(page).to have_content('0') }
        within('#stats-tce') { expect(page).to have_content('3') }
        within('#stats-ce') { expect(page).to have_content('3') }
        within('#stats-te1') { expect(page).to have_content('0') }
        within('#stats-te2') { expect(page).to have_content('0') }
        within('#stats-te3') { expect(page).to have_content('0') }
        within('#stats-te4') { expect(page).to have_content('0') }
        within('#stats-te5') { expect(page).to have_content('0') }
        within('#stats-te6') { expect(page).to have_content('0') }
        within('#stats-te7') { expect(page).to have_content('1') }
        within('#stats-te8') { expect(page).to have_content('1') }
        within('#stats-te9') { expect(page).to have_content('1') }
        within('#stats-te10') { expect(page).to have_content('0') }

        within '#stats-outer-section' do
          expect(page).to have_content('Stats { Current filters: Early Errors: All | Comparison Errors: coga page: application error, clics api: json empty, clics and coga empty values, clics empty value, coga empty value, coga mismatched value, coga incorrect order or missing values | Comm Origins: Joint | Committee Ids: All }')
        end

        expect(page).to_not have_content("Early Errors (we hit a snag and couldn't test the Webpages)")

        within '#filtered-error-items' do
          expect(page).to have_content('Comparison Errors')
          expect(page).to_not have_content('H_COMM1_2021A')
          expect(page).to_not have_content('S_COMM2_2016A')

          within "##{c3.clics_committee_id}" do
            expect(page).to have_content(c3.clics_committee_id, count: 3)
            expect(page).to have_content(ee_c3.sort_error)
            expect(page).to have_content(ee_c3_h1.sort_error)
            expect(page).to have_content(ee_c3_h1.hearing_error_item.hearing.meeting_uid, count: 3)
            expect(page).to have_content(ee_c3_h1_hi1.sort_error)
            expect(page).to have_content(ee_c3_h1_hi1.hearing_item_error_item.hearing_item.hearing_item_uuid, count: 1)
          end
        end
      end
    end
  end
end

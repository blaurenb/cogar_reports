# frozen_string_literal: true

module FeatureHelper
  def waste_seconds(num)
    time = Time.now
    n = 1
    n += 1 until time.advance(seconds: num) < Time.now
  end

  def make_error_items_for_1comm_1hearing_1hi_1vs(report, num, origin_letter, committee_abbrev, session)
    # there is one VS, but there are 2 kinds of error_items, so this will make 2 EI when the other objects just have 1 EI.

    make_error_items_for_1comm_1hearing_1hi(report, num, origin_letter, committee_abbrev, session)

    HearingItem.last.update(votes: [{ 'vote_time' => '02/25/2021 08:44:20 AM' },
                                    { 'vote_time' => '02/25/2021 09:45:09 AM' },
                                    { 'vote_time' => '02/25/2021 10:58:31 AM' }])

    VoteSummary.create!(hearing_item_id: HearingItem.last.id,
                        clics_vote_digest: 'test1',
                        url: 'test',
                        vote_time: '02/25/2021 08:44:20 AM',
                        vote_motion: 'Adopt amendment')
    TargetPage.create!(parentable_id: HearingItem.last.id,
                       parentable_type: 'HearingItem',
                       url_filepath: "committee_meeting_hearing_items/#{HearingItem.last.id}/votes")

    # EI for HI, but having to do with the VS TargetPage
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: TargetPage.last.id,
                      clics_key: 'vote summary order: vote_summaries.vote_time',
                      clics_value: '["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]',
                      sort_error: 8)
    VoteSummaryErrorItem.create!(error_item_id: ErrorItem.last.id, vote_summary_id: VoteSummary.last.id)

    # EI for VS for the VS TargetPage
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: TargetPage.last.id,
                      clics_key: 'vote_summaries.vote_motion',
                      clics_value: VoteSummary.last.vote_motion,
                      sort_error: 8)
    VoteSummaryErrorItem.create!(error_item_id: ErrorItem.last.id, vote_summary_id: VoteSummary.last.id)
  end

  def make_error_items_for_1comm_1hearing_1hi(report, num, origin_letter, committee_abbrev, session)
    comm_name = "#{origin_letter}_#{committee_abbrev}_#{session}"

    # make Committee, Hearing, CommitteeHearing, HearingItem
    Committee.create!(clics_lm_digest: "digest-for-#{comm_name}", clics_committee_id: comm_name)
    Hearing.create!(clics_m_id_digest: "digest-for-#{comm_name}-hearing#{num}",
                    meeting_uid: "#{comm_name}-hearing#{num}")
    CommitteeHearing.create!(committee_id: Committee.last.id, hearing_id: Hearing.last.id)
    HearingItem.create!(hearing_id: Hearing.last.id, hearing_item_uuid: "#{comm_name}-hearing#{num}-hi#{num}")
    TargetPage.create!(parentable_id: Committee.last.id,
                       parentable_type: 'Committee',
                       target_slug: comm_name,
                       url_filepath: "committees/#{origin_letter}/#{session}/#{committee_abbrev}")

    # make 1 ErrorItem for each, then associate with new joins
    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: TargetPage.last.id,
                      clics_key: 'committee_name',
                      clics_value: Committee.last.clics_committee_id,
                      sort_error: 8)
    CommitteeErrorItem.create!(error_item_id: ErrorItem.last.id, committee_id: Committee.last.id)

    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: TargetPage.last.id,
                      clics_key: 'hearing_items count',
                      clics_value: Hearing.last.meeting_uid,
                      sort_error: 8)
    HearingErrorItem.create!(error_item_id: ErrorItem.last.id, hearing_id: Hearing.last.id)

    ErrorItem.create!(reportable_id: report.id,
                      reportable_type: report.type,
                      target_page_id: TargetPage.last.id,
                      clics_key: 'bill_summaries.comm_report_url',
                      clics_value: HearingItem.last.hearing_item_uuid,
                      sort_error: 8)
    HearingItemErrorItem.create!(error_item_id: ErrorItem.last.id, hearing_item_id: HearingItem.last.id)
  end
end

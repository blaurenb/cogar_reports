# frozen_string_literal=>true

class CommitteeData
  def rails_committee
    {"id"=>123,
      "name"=>"State, Civic, Military, & Veterans Affairs",
      "committee_uuid"=>"H_SA_2021A",
      "slug"=>"StateCivicMilitaryandVeteransAffairs",
      "chamber"=>"House",
      "session"=>
        {"id"=>8,
        "title"=>"2021 Regular Session",
        "long_name"=>"First Regular Session | 73rd General Assembly",
        "is_current"=>true,
        "start_date"=>"2021-01-13",
        "end_date"=>"2021-05-15",
        "created_at"=>"2021-01-26T15:01:08.719Z",
        "updated_at"=>"2021-02-05T17:43:01.949Z",
        "short_name"=>"2021a"},
      "committee_memberships"=>
        [{"id"=>1111,
          "legislator_id"=>92,
          "session_id"=>8,
          "committee_id"=>123,
          "role"=>nil,
          "created_at"=>"2021-03-22T21:50:52.187Z",
          "updated_at"=>"2021-03-22T21:50:52.187Z",
          "position"=>nil},
        {"id"=>2222,
          "legislator_id"=>169,
          "session_id"=>8,
          "committee_id"=>123,
          "role"=>nil,
          "created_at"=>"2021-01-27T16:58:40.363Z",
          "updated_at"=>"2021-02-05T17:46:51.863Z",
          "position"=>nil}],
      "type"=>nil,
      "display_on_member_page"=>true,
      "staff_email"=>"Ryan.Dudley@state.co.us",
      "created_at"=>"2021-01-27T16:58:40.268Z",
      "updated_at"=>"2021-03-22T21:50:52.145Z",
      "url"=>"https://cogar.denvertech.org/committees/123.json"}
  end

  def rails_committees
    [
      {"id"=>111,
      "name"=>"State, Civic, Military, & Veterans Affairs",
      "committee_uuid"=>"H_SA_2021A",
      "slug"=>"StateCivicMilitaryandVeteransAffairs",
      "chamber"=>"House",
      "session"=>
        {"id"=>8,
        "title"=>"2021 Regular Session",
        "long_name"=>"First Regular Session | 73rd General Assembly",
        "is_current"=>true,
        "start_date"=>"2021-01-13",
        "end_date"=>"2021-05-15",
        "created_at"=>"2021-01-26T15:01:08.719Z",
        "updated_at"=>"2021-02-05T17:43:01.949Z",
        "short_name"=>"2021a"},
      "committee_memberships"=>
        [{"id"=>111,
          "legislator_id"=>92,
          "session_id"=>8,
          "committee_id"=>111,
          "role"=>nil,
          "created_at"=>"2021-03-22T21:50:52.187Z",
          "updated_at"=>"2021-03-22T21:50:52.187Z",
          "position"=>nil}],
      "type"=>nil,
      "display_on_member_page"=>true,
      "staff_email"=>"Ryan.Dudley@state.co.us",
      "created_at"=>"2021-01-27T16:58:40.268Z",
      "updated_at"=>"2021-03-22T21:50:52.145Z",
      "url"=>"https://cogar.denvertech.org/committees/111.json"},
      {"id"=>222,
      "name"=>"State, Civic, Military, & Veterans Affairs",
      "committee_uuid"=>"H_SA_2019A",
      "slug"=>"StateCivicMilitaryandVeteransAffairs",
      "chamber"=>"Joint Committee",
      "session"=>
        {"id"=>8,
        "title"=>"2019 Regular Session",
        "long_name"=>"First Regular Session | 72nd General Assembly",
        "is_current"=>true,
        "start_date"=>"2019-01-13",
        "end_date"=>"2019-05-15",
        "created_at"=>"2019-01-26T15:01:08.719Z",
        "updated_at"=>"2019-02-05T17:43:01.949Z",
        "short_name"=>"2020a"},
      "committee_memberships"=>
        [{"id"=>222,
          "legislator_id"=>92,
          "session_id"=>8,
          "committee_id"=>222,
          "role"=>nil,
          "created_at"=>"2019-03-22T21:50:52.187Z",
          "updated_at"=>"2019-03-22T21:50:52.187Z",
          "position"=>nil}],
      "type"=>nil,
      "display_on_member_page"=>true,
      "staff_email"=>"Ryan.Dudley@state.co.us",
      "created_at"=>"2019-01-27T16:58:40.268Z",
      "updated_at"=>"2019-03-22T21:50:52.145Z",
      "url"=>"https://cogar.denvertech.org/committees/222.json"}

      ]
  end

  def clics_committee
    JSON.parse('{"committee_name":"CC_2016A","meeting_dates":["2016-04-13T13:42:14Z", "2016-04-13T13:54:41Z", "2016-05-11T12:26:28Z", "2016-05-11T13:18:28Z", "2016-05-11T16:53:05Z"],"meeting_uids":["CC_2016A_20160413_134214", "CC_2016A_20160413_135441"] }')
  end

  def clics_committee_mtg_data_single_hi
    JSON.parse('{"meeting_summary_url": "","meeting_date": "02/11/2021","meeting_time": "01:36:12 PM","committee_id": ["H_ALW_2021A","S_AGR_2021A"],"meeting_uid": "H_ALW_2021A_20210211_133612","meeting_room": "Old State Library","event_url": "https://sg001-harmony.sliq.net/00327/Harmony/en/PowerBrowser/PowerBrowserV2/20210211/-1/10595","members":[],"committee_actions":[],"attachments":[],"bill_summaries":[{"bill_num":"Office of the State Auditor Presentations","summary_title":"Office of the State Auditor Presentations","bill_action":"","hearing_item_uuid":"0E0D0D64DAF593A68725867900716A28","bill_summ_url":"","granicus_url":"","event_url":"","comm_report_url":[],"attachments":[],"vote_summaries":[]},{"bill_num":"SMART Act Presentation from the Department of Agriculture","summary_title":"SMART Act Presentation from the Department of Agriculture","bill_action":"","hearing_item_uuid":"A88B48BA58FBDC578725867900751199","bill_summ_url":"","granicus_url":"","event_url":"","comm_report_url":[],"attachments":[],"vote_summaries":[]} ]}')
  end

  def clics_committee_mtg_data_triple_hi
    JSON.parse('{"meeting_summary_url": "","meeting_date": "02/11/2021","meeting_time": "01:36:12 PM","committee_id": ["H_ALW_2021A","S_AGR_2021A"],"meeting_uid": "H_ALW_2021A_20210211_133612","meeting_room": "Old State Library","event_url": "https://sg001-harmony.sliq.net/00327/Harmony/en/PowerBrowser/PowerBrowserV2/20210211/-1/10595","members": [],"committee_actions": [],"attachments": [],"bill_summaries": [{"bill_num": "Office of the State Auditor Presentations","summary_title": "Office of the State Auditor Presentations","bill_action": "","hearing_item_uuid": "0E0D0D64DAF593A68725867900716A28","bill_summ_url": "www.bill_summ_url.com","granicus_url": "www.granicus_url.com","event_url": "www.event_url.com","comm_report_url": ["www.comm_report_url.com"],"attachments": [],"vote_summaries": [{"url": "www.vote_summaries_url.com"}]},{"bill_num": "SMART Act Presentation from the Department of Agriculture","summary_title": "SMART Act Presentation from the Department of Agriculture","bill_action": "","hearing_item_uuid": "A88B48BA58FBDC578725867900751199","bill_summ_url": "","granicus_url": "","event_url": "","comm_report_url": [],"attachments": [],"vote_summaries": []},{"bill_num": "Public Testimony for the Department of Agriculture","summary_title": "Public Testimony for the Department of Agriculture","bill_action": "","hearing_item_uuid": "94F3A87005F0A7AE8725867900777819","bill_summ_url": "","granicus_url": "","event_url": "","comm_report_url": [],"attachments": [],"vote_summaries": []}]
    }')
  end

  def cogar_all_cmhi
    [
    {"id"=>1,
      "title"=>"Office of the State Auditor Presentations",
      "actions"=>nil,
      "cmhi_uuid"=>"0E0D0D64DAF593A68725867900716A28",
      "bill_id"=>nil,
      "created_at"=>"2021-05-13T15:45:35.261Z",
      "updated_at"=>"2021-05-13T15:45:35.270Z",
      "url"=>"http://localhost:3001/committee_meeting_hearing_items/1.json"},
    {"id"=>2,
      "title"=>"SMART Act Presentation from the Department of Agriculture",
      "actions"=>nil,
      "cmhi_uuid"=>"A88B48BA58FBDC578725867900751199",
      "bill_id"=>nil,
      "created_at"=>"2021-05-13T15:45:35.273Z",
      "updated_at"=>"2021-05-13T15:45:35.276Z",
      "url"=>"http://localhost:3001/committee_meeting_hearing_items/2.json"},
    {"id"=>3,
      "title"=>"Public Testimony for the Department of Agriculture",
      "actions"=>nil,
      "cmhi_uuid"=>"94F3A87005F0A7AE8725867900777819",
      "bill_id"=>nil,
      "created_at"=>"2021-05-13T15:45:35.279Z",
      "updated_at"=>"2021-05-13T15:45:35.282Z",
      "url"=>"http://localhost:3001/committee_meeting_hearing_items/3.json"},
    {"id"=>4,
      "title"=>"SMART Act Presentation from the Department of Agriculture",
      "actions"=>nil,
      "cmhi_uuid"=>"9E25735C713E28B087258679007968CA",
      "bill_id"=>nil,
      "created_at"=>"2021-05-13T15:45:35.284Z",
      "updated_at"=>"2021-05-13T15:45:35.288Z",
      "url"=>"http://localhost:3001/committee_meeting_hearing_items/4.json"}
      ]
  end
end
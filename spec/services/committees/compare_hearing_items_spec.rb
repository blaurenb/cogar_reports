# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/nokogiri_committee_page_data'

RSpec.describe Committees::Comparisons::CompareHearingItems do
  describe 'Methods' do
    let!(:report) { ::Reports::CommitteeReport.create! }
    let!(:committee) { Committee.create!(clics_committee_id: 'J_CBAC_2021A', clics_lm_digest: '123abc') }
    let!(:hearing) do
      Hearing.create!(clics_m_id_digest: 'bcd123', meeting_uid: 'J_CBAC_2021A_20210507_133742',
                      hearing_item_uuids: ['6E73C18D95BF1037872586CE006BD71F'])
    end
    let!(:committee_hearing) { CommitteeHearing.create!(committee_id: committee.id, hearing_id: hearing.id) }
    let!(:hearing_item) do
      HearingItem.create!(hearing_id: hearing.id,
                          hearing_item_uuid: '6E73C18D95BF1037872586CE006BD71F',
                          bill_num: 'bill_num',
                          summary_title: 'Proposed General Rose Statue and Lincoln Park name change',
                          bill_action: 'Amended, referred to the Committee of the Whole',
                          bill_summ_url: 'www.bill_summ_url.com',
                          comm_report_url: 'www.comm_report_url.com',
                          vote_summaries_url: 'www.vote_summaries.com')
    end
    let!(:tp) do
      TargetPage.create!(parentable_id: Committee.first.id, parentable_type: 'Committee', url_filepath: 'url_filepath',
                         target_slug: 'target_slug')
    end
    let!(:nd) { NokogiriCommitteePageData.new }
    let!(:comparables) do
      OpenStruct.new(report: report,
                     tp: tp,
                     doc: nd.html_page,
                     hearing: hearing,
                     hearing_item: hearing_item)
    end
    let!(:ch) { Committees::Comparisons::CompareHearingItems.new(comparables) }
    let!(:ei_struct) do
      OpenStruct
        .new(report: comparables.report,
             tp: comparables.tp,
             clics_key: '',
             clics_value: '',
             target_css: '',
             target_value: '',
             sort_error: 0)
    end

    it 'get_target_value(doc, hearing_item, hi_field)' do
      # summary_title bill_action
      result = ch.get_target_value(comparables.doc, hearing_item, 'summary_title')
      expect(result).to eq('Proposed General Rose Statue and Lincoln Park name change')
      result = ch.get_target_value(comparables.doc, hearing_item, 'bill_action')
      expect(result).to eq('Amended, referred to the Committee of the Whole')
    end

    it 'get_target_value(doc, hearing_item, hi_field) with bad data' do
      # summary_title bill_action
      hearing_item.update(hearing_item_uuid: 'bogus')
      result = ch.get_target_value(comparables.doc, hearing_item, 'summary_title')
      expect(result).to eq('')
      result = ch.get_target_value(comparables.doc, hearing_item, 'bill_action')
      expect(result).to eq('')
    end

    it 'compare_vote_summaries_url(comparables, ei_struct)' do
      expect(ErrorItem.all.length).to eq(0)
      expect(HearingItemErrorItem.all.length).to eq(0)

      ch.compare_vote_summaries_url(comparables, ei_struct)

      expect(ErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingItemErrorItem.last.hearing_item_id).to eq(HearingItem.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('bill_summaries.vote_summaries.url')
      expect(ErrorItem.last.clics_value).to eq('www.vote_summaries.com')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_item_vote_summaries_url')
      expect(ErrorItem.last.target_value).to eq('Votes')
    end

    it 'compare_vote_summaries_url(comparables, ei_struct) with bad data' do
      # kinds of bad data: both blank, one missing
      comparables.doc = nd.html_page_no_data
      ch.compare_vote_summaries_url(comparables, ei_struct)
      expect(ErrorItem.last.sort_error).to eq('coga empty value')
      expect(ErrorItem.last.clics_value).to eq('www.vote_summaries.com')
      expect(ErrorItem.last.target_value).to eq('')

      hearing_item.update(vote_summaries_url: '')
      ch.compare_vote_summaries_url(comparables, ei_struct)
      expect(ErrorItem.last.sort_error).to eq('clics and coga empty values')
      expect(ErrorItem.last.clics_value).to eq('')
      expect(ErrorItem.last.target_value).to eq('')
    end

    it 'compare_fields_presence(comparables, hi_field, ei_struct) for bill_summ_url' do
      ch.compare_fields_presence(comparables, 'bill_summ_url', ei_struct)
      expect(ErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingItemErrorItem.last.hearing_item_id).to eq(HearingItem.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('bill_summaries.bill_summ_url')
      expect(ErrorItem.last.clics_value).to eq('www.bill_summ_url.com')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_item_bill_summ_url')
      expect(ErrorItem.last.target_value).to eq('Hearing Summary')
    end

    it 'compare_fields_presence(comparables, hi_field, ei_struct) for comm_report_url' do
      ch.compare_fields_presence(comparables, 'comm_report_url', ei_struct)
      expect(ErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingItemErrorItem.last.hearing_item_id).to eq(HearingItem.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('bill_summaries.comm_report_url')
      expect(ErrorItem.last.clics_value).to eq('www.comm_report_url.com')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_item_comm_report_url')
      expect(ErrorItem.last.target_value).to eq('Committee Report')
    end

    it 'compare_fields_presence(comparables, hi_field, ei_struct) for comm_report_url with missing data' do
      comparables.doc = nd.html_page_no_data
      ch.compare_fields_presence(comparables, 'comm_report_url', ei_struct)
      expect(ErrorItem.last.sort_error).to eq('coga empty value')
      expect(ErrorItem.last.clics_value).to eq('www.comm_report_url.com')
      expect(ErrorItem.last.target_value).to eq('')

      hearing_item.update!(comm_report_url: '')
      ch.compare_fields_presence(comparables, 'comm_report_url', ei_struct)
      expect(ErrorItem.last.sort_error).to eq('clics and coga empty values')
      expect(ErrorItem.last.clics_value).to eq('')
      expect(ErrorItem.last.target_value).to eq('')
    end

    it 'compare_fields_values(comparables, hi_field, ei_struct) for summary_title' do
      # summary_title
      ch.compare_fields_values(comparables, 'summary_title', ei_struct)
      expect(ErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingItemErrorItem.last.hearing_item_id).to eq(HearingItem.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('bill_summaries.summary_title')
      expect(ErrorItem.last.clics_value).to eq('Proposed General Rose Statue and Lincoln Park name change')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_item_summary_title')
      expect(ErrorItem.last.target_value).to eq('Proposed General Rose Statue and Lincoln Park name change')
    end

    it 'compare_fields_values(comparables, hi_field, ei_struct) for bill_action' do
      # bill_action
      ch.compare_fields_values(comparables, 'bill_action', ei_struct)
      expect(ErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingItemErrorItem.last.hearing_item_id).to eq(HearingItem.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('bill_summaries.bill_action')
      expect(ErrorItem.last.clics_value).to eq('Amended, referred to the Committee of the Whole')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_item_bill_action')
      expect(ErrorItem.last.target_value).to eq('Amended, referred to the Committee of the Whole')
    end

    it 'compare_fields_values(comparables, hi_field,, ei_struct) with missing data' do
      comparables.doc = nd.html_page_no_data
      ch.compare_fields_values(comparables, 'bill_action', ei_struct)
      expect(ErrorItem.last.sort_error).to eq('coga empty value')
      expect(ErrorItem.last.clics_value).to eq('Amended, referred to the Committee of the Whole')
      expect(ErrorItem.last.target_value).to eq('')

      hearing_item.update!(bill_action: '')
      ch.compare_fields_values(comparables, 'bill_action', ei_struct)
      expect(ErrorItem.last.sort_error).to eq('clics and coga empty values')
      expect(ErrorItem.last.clics_value).to eq('')
      expect(ErrorItem.last.target_value).to eq('')
    end

    it 'compare_hearing_item_details(comparables, ei_struct)' do
      ch.compare_hearing_item_details(comparables, ei_struct)
      expect(ErrorItem.all.length).to eq(5)
      expect(HearingItemErrorItem.all.length).to eq(5)
      expect(ErrorItem.where(clics_key: 'bill_summaries.summary_title').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.bill_action').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.bill_summ_url').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.comm_report_url').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.vote_summaries.url').length).to eq(1)
      expect(ErrorItem.where(sort_error: 'ok').length).to eq(5)
    end

    it 'compare_hearing_item_details(comparables, ei_struct)nwith bad data' do
      comparables.doc = nd.html_page_no_data
      ch.compare_hearing_item_details(comparables, ei_struct)
      expect(ErrorItem.all.length).to eq(5)
      expect(HearingItemErrorItem.all.length).to eq(5)
      expect(ErrorItem.where(clics_key: 'bill_summaries.summary_title').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.bill_action').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.bill_summ_url').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.comm_report_url').length).to eq(1)
      expect(ErrorItem.where(clics_key: 'bill_summaries.vote_summaries.url').length).to eq(1)
      expect(ErrorItem.where(sort_error: 'coga empty value').length).to eq(5)
    end
  end
end

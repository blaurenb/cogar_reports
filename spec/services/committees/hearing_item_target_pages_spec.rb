# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/committee_data'

RSpec.describe Committees::HearingItemTargetPages do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:cm) { CommitteeData.new }

    let!(:hearing1) do
      Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: 'abc123',
                      hearing_item_uuids: ['0E0D0D64DAF593A68725867900716A28'])
    end
    let!(:hearing2) do
      Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: 'abc123',
                      hearing_item_uuids: %w[A88B48BA58FBDC578725867900751199 9E25735C713E28B087258679007968CA])
    end
    let!(:hearing3) do
      Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: 'abc123',
                      hearing_item_uuids: ['94F3A87005F0A7AE8725867900777819'])
    end

    let!(:hitp) { Committees::HearingItemTargetPages.new(report) }

    it 'make_endpoint' do
      endpoint = hitp.make_endpoint
      expect(endpoint).to eq('http://cwr-staging.denvertech.org/committee_meeting_hearing_items.json')
    end

    it 'handle_json_data(endpoint, result) without errors' do
      endpoint = 'bogus.com.page'
      result = OpenStruct.new(data: cm.cogar_all_cmhi, error: nil)
      hi_json = hitp.handle_json_data(endpoint, result)
      expect(hi_json).to eq(cm.cogar_all_cmhi)
      expect(EarlyError.all.length).to eq(0)
    end

    it 'handle_json_data(endpoint, result) with error' do
      endpoint = 'bogus.com.page'
      result = OpenStruct.new(data: nil, error: 'Bogus Error')
      hi_json = hitp.handle_json_data(endpoint, result)
      expect(hi_json).to be_nil
      expect(EarlyError.all.length).to eq(1)
      expect(EarlyError.first.description).to include('Could not parse JSON from COGA hearing_item endpoint: bogus.com.page.')
      expect(EarlyError.first.description).to include('Bogus Error')

      result = OpenStruct.new(data: nil, error: nil)
      hi_json = hitp.handle_json_data(endpoint, result)
      expect(hi_json).to be_nil
      expect(EarlyError.all.length).to eq(2)
      expect(EarlyError.first.description).to include('Could not parse JSON from COGA hearing_item endpoint: bogus.com.page.')
      expect(EarlyError.second.description).to include('unknown')
    end

    it 'make_ids_hash(cogar_hearing_items)' do
      uuid_by_cogar_id = hitp.make_ids_hash(cm.cogar_all_cmhi)

      expected = { '0E0D0D64DAF593A68725867900716A28' => 1, '94F3A87005F0A7AE8725867900777819' => 3,
                   '9E25735C713E28B087258679007968CA' => 4, 'A88B48BA58FBDC578725867900751199' => 2 }
      expect(uuid_by_cogar_id).to eq(expected)
    end

    it 'do_target_pages(uuid_by_cogar_id) with good data' do
      expect(TargetPage.all.length).to eq(0)

      uuid_by_cogar_id = hitp.make_ids_hash(cm.cogar_all_cmhi)
      hitp.do_target_pages(uuid_by_cogar_id)

      expect(TargetPage.all.length).to eq(4)
      expect(EarlyError.all.length).to eq(0)
    end

    it 'build_url_filepath(uuid, id) with good data' do
      url = hitp.build_url_filepath('0E0D0D64DAF593A68725867900716A28', 1)
      expect(url).to eq('committee_meeting_hearing_items/1/votes')
      expect(EarlyError.all.length).to eq(0)
    end

    it 'build_url_filepath(uuid, id) with bad data' do
      url = hitp.build_url_filepath('', 1)
      expect(url).to be_nil
      expect(EarlyError.all.length).to eq(1)
      msg = 'Failed to find/create TargetPage for HearingItem due to missing data. Coga CommitteeMeetingHearingItem id: 1 and uuid: '
      expect(EarlyError.first.description).to eq(msg)

      url = hitp.build_url_filepath('0E0D0D64DAF593A68725867900716A28', nil)
      expect(url).to be_nil
      expect(EarlyError.all.length).to eq(2)
      msg = 'Failed to find/create TargetPage for HearingItem due to missing data. Coga CommitteeMeetingHearingItem id:  and uuid: 0E0D0D64DAF593A68725867900716A28'
      expect(EarlyError.second.description).to eq(msg)
    end

    it 'get_db_hearing_item(hearing_item_uuid)' do
      expect(HearingItem.all.length).to eq(0)

      hitp.get_db_hearing_item('A88B48BA58FBDC578725867900751199')
      expect(HearingItem.all.length).to eq(1)
    end

    it 'get_db_hearing_item(hearing_item_uuid) with exsiting HearingItem' do
      hearing_item = HearingItem.create(hearing_id: hearing2.id,
                                        hearing_item_uuid: 'A88B48BA58FBDC578725867900751199')
      expect(HearingItem.all.length).to eq(1)

      hitp.get_db_hearing_item('A88B48BA58FBDC578725867900751199')
      expect(HearingItem.all.length).to eq(1)
      expect(HearingItem.first.id).to eq(hearing_item.id)
    end

    it 'update_target_page(hearing_item, url_filepath) with good data' do
      expect(TargetPage.all.length).to eq(0)

      hearing_item = HearingItem.create(hearing_id: hearing2.id,
                                        hearing_item_uuid: 'A88B48BA58FBDC578725867900751199')
      url_filepath = 'bogus.com/page'
      hitp.update_target_page(hearing_item, url_filepath)

      expect(TargetPage.all.length).to eq(1)
      expect(EarlyError.all.length).to eq(0)
    end

    it 'update_target_page(hearing_item, url_filepath) with bad data' do
      expect(TargetPage.all.length).to eq(0)

      hearing_item = HearingItem.create(hearing_id: hearing2.id,
                                        hearing_item_uuid: 'A88B48BA58FBDC578725867900751199')
      url_filepath = nil
      hitp.update_target_page(hearing_item, url_filepath)

      expect(TargetPage.all.length).to eq(0)
      expect(EarlyError.all.length).to eq(1)

      msg1 =  'Failed to find/create TargetPage for Cogar CMHI id: A88B48BA58FBDC578725867900751199.'
      msg2 =  "Url filepath can't be blank"

      expect(EarlyError.first.description).to include(msg1)
      expect(EarlyError.first.description).to include(msg2)
    end
  end
end

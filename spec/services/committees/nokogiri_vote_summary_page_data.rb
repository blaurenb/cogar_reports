# frozen_string_literal: true

class NokogiriVoteSummaryPageData
  def html_page
    # parse the raw html of a page into a Nokogiri Document
    Nokogiri::HTML('

      <html lang="en"><head><style type="text/css" data-tag-name="trix-editor">trix-editor {
      display: block;
      }

      trix-editor:empty:not(:focus)::before {
      content: attr(placeholder);
      color: graytext;
      cursor: text;
      pointer-events: none;
      }

      trix-editor a[contenteditable=false] {
      cursor: text;
      }

      trix-editor img {
      max-width: 100%;
      height: auto;
      }

      trix-editor [data-trix-attachment] figcaption textarea {
      resize: none;
      }

      trix-editor [data-trix-attachment] figcaption textarea.trix-autoresize-clone {
      position: absolute;
      left: -9999px;
      max-height: 0px;
      }

      trix-editor [data-trix-attachment] figcaption[data-trix-placeholder]:empty::before {
      content: attr(data-trix-placeholder);
      color: graytext;
      }

      trix-editor [data-trix-cursor-target] {
      display: inline-block !important;
      width: 1px !important;
      padding: 0 !important;
      margin: 0 !important;
      border: none !important;
      }

      trix-editor [data-trix-cursor-target=left] {
      vertical-align: top !important;
      margin-left: -1px !important;
      }

      trix-editor [data-trix-cursor-target=right] {
      vertical-align: bottom !important;
      margin-right: -1px !important;
      }</style><style type="text/css" data-tag-name="trix-toolbar">trix-toolbar {
      display: block;
      }

      trix-toolbar {
      white-space: nowrap;
      }

      trix-toolbar [data-trix-dialog] {
      display: none;
      }

      trix-toolbar [data-trix-dialog][data-trix-active] {
      display: block;
      }

      trix-toolbar [data-trix-dialog] [data-trix-validate]:invalid {
      background-color: #ffdddd;
      }</style>
      <title>Colorado General Assembly</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta charset="utf-8">
      <meta name="csrf-param" content="authenticity_token">
      <meta name="csrf-token" content="PooqLDmpbejLOPFyWdc8jXjQhCaX+5sO3d/A+NgKp8KA8XtIDQoJvt7F152c3C35p/CHuyyk/VY8yvBiNuTxjw==">

      <link rel="stylesheet" media="screen" href="/assets/application.debug-f3e8aa25decfe048f332b129cf0dfb87f9357c100d1b23aa705d3ef5fcf1c43a.css">
      <script src="/packs/js/application-9a204fd90563014c030b.js"></script>
      </head>

      <body>
      <a class="skip-main" href="#main">Skip to main content</a>
      <div class="header-wrapper">
      <nav class="navbar navbar-light bg-light">
      <div class="container">
      <span class="nav-brand">
      <a class="navbar-brand" href="/">
      <span class="site_logo"><img src="/images/cga-logo-med.png" class="d-inline-block align-top" alt=""></span>
      <span class="site_title"><h1>Colorado General Assembly</h1></span>
      </a>
      </span>
      <span class="nav-search">
      <form class="form-inline my-2 my-lg-0" method="get" action="/search">
      <input class="form-control mr-sm-2" type="search" value="" name="q" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
      </span>
      </div>
      </nav>
      <nav class="navbar navbar-expand-md navbar-light bg-light" id="navbarSupportedContent">
      <div class="container">
      <div class="" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Schedule
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="/session-schedule">Schedule</a>

      </div>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="/bills">Bills</a>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="">Laws</a>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="/legislators">Legislators</a>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="/initiatives">Initiatives</a>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="/committees">Committees</a>
      </li>

      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Initiatives
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="/initiatives">Initiatives</a>

      <a class="dropdown-item" href="/ballots">Ballot Analysis</a>

      </div>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="">Budget</a>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="">Audits</a>
      </li>

      <li class="nav-item">
      <a class="nav-link" href="/publications">Publications</a>
      </li>

      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Agencies
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="/agencies/house-of-representatives">House of Representatives</a>

      <a class="dropdown-item" href="/agencies/house-of-representatives">House of Representative</a>

      <a class="dropdown-item" href="/agencies/senate">Senate</a>

      <a class="dropdown-item" href="/agencies/office-of-the-state-auditor">Office of the State Auditor</a>

      <a class="dropdown-item" href="/agencies/office-of-legislative-legal-services">Office of Legislative Legal Services</a>

      <a class="dropdown-item" href="/agencies/legislative-council-staff">Legislative Council Staff</a>

      <a class="dropdown-item" href="/agencies/joint-budget-committee">Joint Budget Committee</a>

      <a class="dropdown-item" href="/agencies/joint-budget-committee">Joint Budget Committee Staff</a>

      </div>
      </li>


      </ul>


      </div>
      </div>
      </nav>

      </div>
      <div class="main-wrapper" id="main">
      <div class="page-wrapper">
      <p class="notice"></p>
      <p class="alert"></p>

      <center>
      <h3>Vote Summary</h3>
      <h4>HB21-1102 - Consumer Protection For Dog &amp; Cat Purchasers</h4>

      <h4>HOUSE COMMITTEE ON AGRICULTURE, LIVESTOCK, &amp; WATER</h4>
      <h5>Date: Feb 25, 2021</h5>
      <h5>Location: HCR 0107</h5>
      </center>
      <table class="table mb-0 mt-5" id="votes-table" style="width:100%">
      <tbody><tr>
      <th>ACTIVITY</th>
      <th></th>
      <th>VOTE</th>
      <th class="pull-right documents-header">DOCUMENTS</th>
      </tr>
      </tbody></table>
      <div class="vote-items" id="accordionExample">
      <div class="vote_item cwr_vote_item" id="cwr_20210225104420">
      <a data-toggle="collapse" id="top1" data-target="#Vote1" class="collapsed">
      <h3>
      <div class="row vote-accordion-header">
      <div class="col-5 cwr_vote_motion_header">Adopt amendment L.003 (Attachment A).</div>
      <div class="col-5">The motion passed on a vote of 7-4.</div>
      <div class="col-2 vote-icon-div flex-bind">Vote Summary <i class="fa fa-chevron-up pull-right" aria-hidden="true"></i></div>
      </div>
      </h3>
      </a>
      <div class="collapse " id="Vote1" data-parent="#accordionExample">
      <table class="table table-striped vote-header-table">
      <tbody><tr>
      <td>BILL:</td>
      <td>HB21-1102</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>TIME:</td>
      <td class="cwr_vote_time">02/25/2021 10:44 AM</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>MOVED:</td>
      <td>Arndt</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>MOTION:</td>
      <td class="cwr_vote_motion">Adopt amendment L.003 (Attachment A).</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>SECONDED:</td>
      <td>McCormick</td>
      <td></td>
      <td></td>
      </tr>
      </tbody></table>
      <table class="table table-member-votes">
      <tbody><tr>
      <td></td>
      <td></td>
      <td></td>
      <td>VOTE</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Arndt</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Catlin</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Cutter</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Holtorf</td>
      <td>No</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Lontine</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>McCormick</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>McLachlan</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Pelton</td>
      <td>No</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Roberts</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Valdez</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Will</td>
      <td>No</td>
      </tr>
      </tbody></table>

      <table class="table">
      <tbody><tr>
      <td class="vote-result" colspan="4">
      YES: 7,
      NO: 4,
      ABS: 0,
      EXC: 0,
      FINAL ACTION: <span id="final-action">PASS</span>
      </td>
      </tr>
      </tbody></table>


      </div>
      </div>

      <div class="vote_item cwr_vote_item" id="cwr_20210225104509">
      <a data-toggle="collapse" id="top2" data-target="#Vote2" class="collapsed">
      <h3>
      <div class="row vote-accordion-header">
      <div class="col-5 cwr_vote_motion_header">Adopt amendment L.005 (Attachment B).</div>
      <div class="col-5">The motion passed without objection.</div>
      <div class="col-2 vote-icon-div flex-bind">Vote Summary <i class="fa fa-chevron-up pull-right" aria-hidden="true"></i></div>
      </div>
      </h3>
      </a>
      <div class="collapse " id="Vote2" data-parent="#accordionExample">
      <table class="table table-striped vote-header-table">
      <tbody><tr>
      <td>BILL:</td>
      <td>HB21-1102</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>TIME:</td>
      <td class="cwr_vote_time">02/25/2021 10:45 AM</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>MOVED:</td>
      <td>Roberts</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>MOTION:</td>
      <td class="cwr_vote_motion">Adopt amendment L.005 (Attachment B).</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>SECONDED:</td>
      <td>Holtorf</td>
      <td></td>
      <td></td>
      </tr>
      </tbody></table>
      <table class="table table-member-votes">
      <tbody><tr>
      <td></td>
      <td></td>
      <td></td>
      <td>VOTE</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Arndt</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Catlin</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Cutter</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Holtorf</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Lontine</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>McCormick</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>McLachlan</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Pelton</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Roberts</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Valdez</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Will</td>
      <td></td>
      </tr>
      </tbody></table>

      <table class="table">
      <tbody><tr>
      <td class="vote-result" colspan="4">
      YES: 0,
      NO: 0,
      ABS: 0,
      EXC: 0,
      FINAL ACTION: <span id="final-action">Pass Without Objection</span>
      </td>
      </tr>
      </tbody></table>


      </div>
      </div>

      <div class="vote_item cwr_vote_item" id="cwr_20210225105831">
      <a data-toggle="collapse" id="top3" data-target="#Vote3" class="collapsed">
      <h3>
      <div class="row vote-accordion-header">
      <div class="col-5 cwr_vote_motion_header">Refer House Bill 21-1102, as amended, to the Committee of the Whole.</div>
      <div class="col-5">The motion passed on a vote of 7-4.</div>
      <div class="col-2 vote-icon-div flex-bind">Vote Summary <i class="fa fa-chevron-up pull-right" aria-hidden="true"></i></div>
      </div>
      </h3>
      </a>
      <div class="collapse " id="Vote3" data-parent="#accordionExample">
      <table class="table table-striped vote-header-table">
      <tbody><tr>
      <td>BILL:</td>
      <td>HB21-1102</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>TIME:</td>
      <td class="cwr_vote_time">02/25/2021 10:58 AM</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>MOVED:</td>
      <td>Arndt</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>MOTION:</td>
      <td class="cwr_vote_motion">Refer House Bill 21-1102, as amended, to the Committee of the Whole.</td>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td>SECONDED:</td>
      <td>McCormick</td>
      <td></td>
      <td></td>
      </tr>
      </tbody></table>
      <table class="table table-member-votes">
      <tbody><tr>
      <td></td>
      <td></td>
      <td></td>
      <td>VOTE</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Arndt</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Catlin</td>
      <td></td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Cutter</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Holtorf</td>
      <td>No</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Lontine</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>McCormick</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>McLachlan</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Pelton</td>
      <td>No</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Roberts</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Valdez</td>
      <td>Yes</td>
      </tr>
      <tr>
      <td></td>
      <td></td>
      <td>Will</td>
      <td>No</td>
      </tr>
      </tbody></table>

      <table class="table">
      <tbody><tr>
      <td class="vote-result" colspan="4">
      YES: 7,
      NO: 4,
      ABS: 0,
      EXC: 0,
      FINAL ACTION: <span id="final-action">PASS</span>
      </td>
      </tr>
      </tbody></table>
      </div>
      </div>
      </div>
      </div>
      </div>
      <div class="footer-wrapper">
      <div class="footer">
      <ul class="flex-container">
      <li class="flex-item">
      <h4 class="block-title">Colorado General Assembly</h4>
      <div class="field-item even">
      <div>Colorado General Assembly</div>
      <div>200 E&nbsp;Colfax Avenue</div>
      <div>Denver, CO 80203</div>
      <p><a href="mailto:comments.ga@state.co.us" class="mailto">comments.ga@state.co.us<span class="mailto"><span class="element-invisible"> (link sends e-mail)</span></span></a></p>
      </div>

      </li>
      <li class="flex-item">
      Resources &amp; Information
      </li>
      <li class="flex-item">
      Policies
      </li>
      <li class="flex-item">
      For Legislators &amp; Staff
      <div class="columns ">

      <p class="mt-2">
      <a class="btn btn-success" href="/users/sign_in">Login</a>
      </p>
      </div>
      </li>
      </ul>
      </div>
      </div>


      </body></html>

    ')
  end
end

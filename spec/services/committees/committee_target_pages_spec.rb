# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/committee_data'

RSpec.describe Committees::CommitteeTargetPages do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:cfn) { Committees::CommitteeTargetPages.new(report) }
    let!(:cm) { CommitteeData.new }
    let!(:rails_good_committees) { cm.rails_committees }
    let!(:rails_good_committee) { cm.rails_committee }

    it 'creates an instance with a valid report' do
      expect(Committees::CommitteeTargetPages.new(report)).to be_instance_of(Committees::CommitteeTargetPages)
    end

    it 'make_endpoint' do
      endpoint = cfn.make_endpoint
      expect(endpoint).to eq('http://cwr-staging.denvertech.org/committees.json?sess=all')
    end

    # it 'get_committees' do
    #   # Not tested yet because it calls GetJson. Need to mock or stub that call, so that I can test the remaining functionality of creating an EarlyError if the json is bad
    # end

    context 'build_filepath(committee)' do
      it 'returns a string with valid arguments' do
        url_path = cfn.build_filepath(rails_good_committee)
        expected_path = 'committees/2021a/house/StateCivicMilitaryandVeteransAffairs'
        expect(url_path).to eq(expected_path)
      end

      it 'returns nil and creates errors with bad args: chamber' do
        rails_bad_committee = rails_good_committee
        rails_bad_committee['chamber'] = nil

        url_path = cfn.build_filepath(rails_bad_committee)

        expect(url_path).to eq(nil)
        expect(EarlyError.last.reportable).to eq(report)
        expect(EarlyError.last.description).to include('Failed to find/create TargetPage for Committee')
        expect(EarlyError.last.sort_error).to eq('coga api: json error')
      end

      it 'returns nil and creates errors with bad args: session' do
        rails_bad_committee = rails_good_committee
        rails_bad_committee['session']['short_name'] = nil

        url_path = cfn.build_filepath(rails_bad_committee)

        expect(url_path).to eq(nil)
        expect(EarlyError.last.reportable).to eq(report)
        expect(EarlyError.last.description).to include('Failed to find/create TargetPage for Committee')
        expect(EarlyError.last.sort_error).to eq('coga api: json error')
      end

      it 'returns nil and creates errors with bad args: slug' do
        rails_bad_committee = rails_good_committee
        rails_bad_committee['slug'] = nil

        url_path = cfn.build_filepath(rails_bad_committee)

        expect(url_path).to eq(nil)
        expect(EarlyError.last.reportable).to eq(report)
        expect(EarlyError.last.description).to include('Failed to find/create TargetPage for Committee')
        expect(EarlyError.last.sort_error).to eq('coga api: json error')
      end
    end

    it 'update_target_page() with bad data' do
      rails_json = rails_good_committee
      digest0 = '1234abcd'
      db_committee = Committee.create!(clics_committee_id: rails_json['committee_uuid'],
                                       meeting_dates: %w[2000-01-01T11:11:11.111Z],
                                       meeting_uids: %w[abc123],
                                       clics_lm_digest: digest0)
      url_filepath = ''
      cfn.update_target_page(db_committee, url_filepath, rails_json.dig('slug'))
      expect(TargetPage.all.length).to eq(0)
      expect(EarlyError.first.reportable).to eq(report)
      expect(EarlyError.first.description).to include('Failed to find/create TargetPage for slug')
      expect(EarlyError.first.sort_error).to eq('system: error saving object')
    end

    it 'get_db_committee() when no committee exists' do
      expect(Committee.all.length).to eq(0)
      cfn.get_db_committee('J_ECLC_2021A')
      expect(Committee.all.length).to eq(1)
    end

    it 'validate_existing_target_pages(committees)' do
      rails_json = rails_good_committees
      digest0 = '1234abcd'
      digest1 = '4321abcd'
      Committee.create!(clics_committee_id: rails_json[0]['committee_uuid'],
                        meeting_dates: %w[2000-01-01T11:11:11.111Z],
                        meeting_uids: %w[abc123],
                        clics_lm_digest: digest0)
      Committee.create!(clics_committee_id: rails_json[1]['committee_uuid'],
                        meeting_dates: %w[2000-02-01T11:11:11.111Z],
                        meeting_uids: %w[cba123],
                        clics_lm_digest: digest1)

      cfn.validate_existing_target_pages(rails_json)

      expect(TargetPage.all.length).to eq(2)
      expect(EarlyError.all.length).to eq(0)
      expect(TargetPage.first.parentable).to eq(Committee.first)
      expect(TargetPage.second.parentable).to eq(Committee.second)
    end
  end
end

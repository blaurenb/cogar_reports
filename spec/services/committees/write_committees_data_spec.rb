# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Committees::WriteCommitteesData do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }

    it 'performs write_committee with new data' do
      committees = '[
                      {
                      "committee_name": "CC_HB21-1115_2021A",
                      "meeting_dates": ["2021-05-05T12:03:30Z"],
                      "meeting_uids": ["CC_HB21-1115_2021A_20210505_120330"]
                      },
                      {
                      "committee_name": "CC_SB21-077_2021A",
                      "meeting_dates": ["2021-04-30T14:01:19Z"],
                      "meeting_uids": ["CC_SB21-077_2021A_20210430_140119"]
                      }
                    ]'
      expect(Committee.all.length).to eq(0)
      Committees::WriteCommitteesData.new(report, JSON.parse(committees)).call
      expect(Committee.all.length).to eq(2)
      expect(Committee.first.clics_committee_id).to eq('CC_HB21-1115_2021A')
      expect(Committee.first.meeting_dates).to eq(['2021-05-05T12:03:30Z'])
      expect(Committee.first.meeting_uids).to eq(['CC_HB21-1115_2021A_20210505_120330'])
    end

    it 'performs write_committee with blank data' do
      committees = '[
                      {
                      "committee_name": "CC_HB21-1115_2021A",
                      "meeting_dates": [],
                      "meeting_uids": []
                      }
                    ]'
      expect(Committee.all.length).to eq(0)
      wcd = Committees::WriteCommitteesData.new(report, JSON.parse(committees)).call

      expect(Committee.all.length).to eq(1)
      expect(Committee.first.clics_committee_id).to eq('CC_HB21-1115_2021A')
      expect(Committee.first.meeting_dates).to eq([])
      expect(Committee.first.meeting_uids).to eq([])
    end

    it 'performs write_committee with existing committee' do
      committees = '[
                {
                "committee_name": "CC_HB21-1115_2021A",
                "meeting_dates": ["2021-05-05T12:03:30Z"],
                "meeting_uids": ["CC_HB21-1115_2021A_20210505_120330"]
                },
                {
                "committee_name": "CC_HB21-1115_2021A",
                "meeting_dates": ["2021-05-05T12:03:30Z"],
                "meeting_uids": ["CC_HB21-1115_2021A_20210505_120330"]
                }
              ]'
      expect(Committee.all.length).to eq(0)
      wcd = Committees::WriteCommitteesData.new(report, JSON.parse(committees)).call

      expect(Committee.all.length).to eq(1)
      expect(Committee.first.clics_committee_id).to eq('CC_HB21-1115_2021A')
    end

    it 'performs write_committee with bad data' do
      committees = '[
                {
                "committee_name": "",
                "meeting_dates": ["2021-05-05T12:03:30Z"],
                "meeting_uids": ["CC_HB21-1115_2021A_20210505_120330"]
                }
              ]'
      expect(Committee.all.length).to eq(0)
      expect(EarlyError.all.length).to eq(0)
      wcd = Committees::WriteCommitteesData.new(report, JSON.parse(committees)).call

      expect(Committee.all.length).to eq(0)
      expect(EarlyError.all.length).to eq(1)
      expect(EarlyError.first.sort_error).to eq('system: error saving object')
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/nokogiri_committee_page_data'

RSpec.describe Committees::Comparisons::CompareHearings do
  describe 'Methods' do
    let!(:report) { ::Reports::CommitteeReport.create! }
    let!(:committee) { Committee.create!(clics_committee_id: 'J_CBAC_2021A', clics_lm_digest: '123abc') }
    let!(:hearing) do
      Hearing.create!(clics_m_id_digest: 'bcd123', meeting_uid: 'J_CBAC_2021A_20210507_133742',
                      hearing_item_uuids: ['6E73C18D95BF1037872586CE006BD71F'])
    end
    let!(:committee_hearing) { CommitteeHearing.create!(committee_id: committee.id, hearing_id: hearing.id) }
    let!(:tp) do
      TargetPage.create!(parentable_id: Committee.first.id, parentable_type: 'Committee', url_filepath: 'url_filepath',
                         target_slug: 'target_slug')
    end
    let!(:nd) { NokogiriCommitteePageData.new }
    let!(:comparables) do
      OpenStruct.new(report: report,
                     tp: tp,
                     doc: nd.html_page,
                     hearing: hearing,
                     hearing_item: nil)
    end
    let!(:ch) { Committees::Comparisons::CompareHearings.new(comparables) }
    let!(:ei_struct) do
      OpenStruct
        .new(report: comparables.report,
             tp: comparables.tp,
             clics_key: '',
             clics_value: '',
             target_css: '',
             target_value: '',
             sort_error: 0)
    end

    it 'get_target_values(doc, hearing) with good data' do
      result = ch.get_target_values(comparables.doc, hearing)
      expect(result).to eq(['6E73C18D95BF1037872586CE006BD71F'])

      hearing.update(meeting_uid: 'J_CBAC_2021A_20210219_133555')
      result = ch.get_target_values(comparables.doc, hearing)
      expect(result).to eq(%w[1CADF2211380B6FA8725868100713ECC 782A767710E6E90C87258681007452E0
                              8B62BC85CA9E9AEA8725868100757FC1])
    end

    it 'get_target_values(doc, hearing) with good data' do
      hearing.update(meeting_uid: 'bogus')
      result = ch.get_target_values(comparables.doc, hearing)
      expect(result).to eq([])
    end

    it 'compare_hearing_items_order(comparables, ei_struct)' do
      expect(ErrorItem.all.length).to eq(0)
      expect(HearingErrorItem.all.length).to eq(0)

      ch.compare_hearing_items_order(comparables, ei_struct)
      expect(ErrorItem.all.length).to eq(1)
      expect(HearingErrorItem.all.length).to eq(1)
      expect(HearingErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingErrorItem.last.hearing_id).to eq(Hearing.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('hearing_items order: bill_summaries.hearing_item_uuid')
      expect(ErrorItem.last.clics_value).to eq('["6E73C18D95BF1037872586CE006BD71F"]')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_item_uuids')
      expect(ErrorItem.last.target_value).to eq('["6E73C18D95BF1037872586CE006BD71F"]')
    end

    it 'compare_hearing_items_order(comparables, ei_struct)' do
      hearing.update(meeting_uid: 'J_CBAC_2021A_20210219_133555',
                     hearing_item_uuids: %w[1CADF2211380B6FA8725868100713ECC 782A767710E6E90C87258681007452E0
                                            8B62BC85CA9E9AEA8725868100757FC1])
      comparables.hearing = hearing
      expect(ErrorItem.all.length).to eq(0)
      expect(HearingErrorItem.all.length).to eq(0)

      ch.compare_hearing_items_order(comparables, ei_struct)
      expect(ErrorItem.all.length).to eq(1)
      expect(HearingErrorItem.all.length).to eq(1)
      expect(HearingErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingErrorItem.last.hearing_id).to eq(Hearing.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('hearing_items order: bill_summaries.hearing_item_uuid')
      expect(ErrorItem.last.clics_value).to eq('["1CADF2211380B6FA8725868100713ECC", "782A767710E6E90C87258681007452E0", "8B62BC85CA9E9AEA8725868100757FC1"]')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_item_uuids')
      expect(ErrorItem.last.target_value).to eq('["1CADF2211380B6FA8725868100713ECC", "782A767710E6E90C87258681007452E0", "8B62BC85CA9E9AEA8725868100757FC1"]')
    end

    it 'compare_hearing_items_order(comparables, ei_struct) with mismatched order' do
      hearing.update(meeting_uid: 'J_CBAC_2021A_20210219_133555',
                     hearing_item_uuids: %w[782A767710E6E90C87258681007452E0 1CADF2211380B6FA8725868100713ECC
                                            8B62BC85CA9E9AEA8725868100757FC1])
      # comparables.hearing = hearing

      ch.compare_hearing_items_order(comparables, ei_struct)
      expect(ErrorItem.last.sort_error).to eq('coga mismatched value')
    end

    it 'compare_hearing_items_order(comparables, ei_struct) with bad or missing data' do
      comparables.doc = nd.html_page_no_data
      ch.compare_hearing_items_order(comparables, ei_struct)
      expect(ErrorItem.last.sort_error).to eq('coga empty value')

      hearing.update(meeting_uid: 'bogus', hearing_item_uuids: [])
      ch.compare_hearing_items_order(comparables, ei_struct)
      expect(ErrorItem.last.sort_error).to eq('clics and coga empty values')
    end

    it 'get_target_value_count(doc, hearing)' do
      hearing.update(meeting_uid: 'J_CBAC_2021A_20210219_133555')
      result = ch.get_target_value_count(comparables.doc, hearing)
      expect(result).to eq(3)
    end

    it 'get_target_value_count(doc, hearing) with bad data' do
      hearing.update(meeting_uid: 'bogus')
      result = ch.get_target_value_count(comparables.doc, hearing)
      expect(result).to eq(0)
    end

    it 'compare_hearing_items_count(comparables, ei_struct)' do
      hearing.update(meeting_uid: 'J_CBAC_2021A_20210219_133555',
                     hearing_item_uuids: %w[1CADF2211380B6FA8725868100713ECC 782A767710E6E90C87258681007452E0
                                            8B62BC85CA9E9AEA8725868100757FC1])
      comparables.hearing = hearing
      expect(ErrorItem.all.length).to eq(0)
      expect(HearingErrorItem.all.length).to eq(0)

      ch.compare_hearing_items_count(comparables, ei_struct)
      expect(ErrorItem.all.length).to eq(1)
      expect(HearingErrorItem.all.length).to eq(1)
      expect(HearingErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingErrorItem.last.hearing_id).to eq(Hearing.last.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('hearing_items count')
      expect(ErrorItem.last.clics_value).to eq('3')
      expect(ErrorItem.last.target_css).to eq('.cwr_hearing_J_CBAC_2021A_20210219_133555')
      expect(ErrorItem.last.target_value).to eq('3')
    end

    it 'compare_hearing_items_count(comparables, ei_struct) with empty data' do
      hearing.update(meeting_uid: 'bogus', hearing_item_uuids: [])

      ch.compare_hearing_items_count(comparables, ei_struct)
      expect(HearingErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingErrorItem.last.hearing_id).to eq(Hearing.last.id)
      expect(ErrorItem.last.sort_error).to eq('clics and coga empty values')
      expect(ErrorItem.last.clics_value).to eq('0')
      expect(ErrorItem.last.target_value).to eq('0')
    end

    it 'compare_hearing_items_count(comparables, ei_struct) with bad or missing data' do
      comparables.doc = nd.html_page_no_data

      ch.compare_hearing_items_count(comparables, ei_struct)
      expect(HearingErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingErrorItem.last.hearing_id).to eq(Hearing.last.id)
      expect(ErrorItem.last.sort_error).to eq('coga mismatched value')
      expect(ErrorItem.last.clics_value).to eq('1')
      expect(ErrorItem.last.target_value).to eq('0')
    end

    # it '' do
    #   result = ch.xxx
    #   expect(result).to eq('')
    # end
  end
end

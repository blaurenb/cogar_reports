# frozen_string_literal: true

# frozen_string_literal=>true

class NokogiriCommitteePageData
  def html_page
    # parse the raw html of a page into a Nokogiri Document
    Nokogiri::HTML('

      <html lang="en"><head>
      <title>Capitol Building Advisory Committee | Colorado General Assembly</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta charset="utf-8">
      <meta name="csrf-param" content="authenticity_token">
      <meta name="csrf-token" content="ZZ+qbkH5ZQ7K036zAq0HDt+X45ujOD403YjW+HKlMXJHQtYAU/qhcwRHZQ+KZgLcdq4kWdJiTtNf0D1O0Ym/5A==">
      <link rel="stylesheet" media="screen" href="/assets/application.debug-b322bb8290d6ff0a01c47665cbd92caa09e64e437279840e883ea4b562c89105.css">
      <script src="/packs/js/application-a8202acccaafc2e6f885.js"></script>
      </head>
      <body>
      <a class="skip-main" href="#main">Skip to main content</a>
      <div class="header-wrapper">
      <nav class="navbar navbar-light bg-light">
      <div class="container">
      <span class="nav-brand">
      <a class="navbar-brand" href="/">
      <span class="site_logo"><img src="/images/cga-logo-med.png" class="d-inline-block align-top" alt=""></span>
      <span class="site_title"><h1>Colorado General Assembly</h1></span>
      </a>
      </span>
      <span class="nav-search">
      <form class="form-inline my-2 my-lg-0" method="get" action="/search">
      <input class="form-control mr-sm-2" type="search" value="" name="q" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
      </span>
      </div>
      </nav>
      <nav class="navbar navbar-expand-md navbar-light bg-light" id="navbarSupportedContent">
      <div class="container">
      <div class="" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Schedule
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="/session-schedule">Schedule</a>
      </div>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/bills">Bills</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/legislators">Legislators</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/committees">Committees</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/initiatives">Initiatives</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/publications">Publications</a>
      </li>
      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Agencies
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="/agencies/house-of-representatives">House of Representative</a>
      <a class="dropdown-item" href="/agencies/senate">Senate</a>
      <a class="dropdown-item" href="/agencies/office-of-the-state-auditor">Office of the State Auditor</a>
      <a class="dropdown-item" href="/agencies/office-of-legislative-legal-services">Office of Legislative Legal Services</a>
      <a class="dropdown-item" href="/agencies/legislative-council-staff">Legislative Council Staff</a>
      <a class="dropdown-item" href="/agencies/joint-budget-committee">Joint Budget Committee</a>
      </div>
      </li>
      </ul>
      </div>
      </div>
      </nav>
      </div>
      <div class="main-wrapper" id="main">
      <div class="page-wrapper">
      <p class="notice"></p>
      <p class="alert"></p>
      <div class="row grid-x">
      <div class="medium-8 cells columns col-md-8 committee-top">
      <h1>Capitol Building Advisory Committee</h1>
      <div class="description">The Capitol Building Advisory Committee is comprised of legislative and non-legislative members and is charged with reviewing and advising for the protection of the Capitol buildings historic character, including its art, memorials, furniture, and architectural fixtures.</div>
      </div>
      <div class="medium-4 columns cells col-md-4 comm-desc">
      <p>
      <a href="/committee_audio/4">Committee Audio</a>
      </p>
      <p>
      <strong>Committee:</strong>
      <span id="cwr_clics_comm_id">
      J_CBAC_2021A
      </span>
      </p>
      <p>
      <strong>Chamber</strong>
      Joint Committee
      </p>
      <p>
      <strong>Committee Type:</strong>
      Year Round Committee
      </p>
      <p>
      <strong>Session:</strong>
      2021 Regular Session
      </p>
      <p>
      <strong>Staff email:</strong>
      CBAC@state.co.us
      </p>
      </div>
      <div class="row committee-details ">
      </div>
      </div>
      <div class="committee-members">
      <p class="medium-text-center committee-members-header">Committee Members</p>
      <div class="row grid-x">
      <div class="member large-4 col-md-6 col-lg-4 medium-6 columns cells">
      <div class="circular"><a href="/legislators/42"><img width="100" height="100" class="legislator-avatar" src="http://localhost:3000/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBMdz09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--4cf2a0bee392ea2b55173ce9f7a73e14b211ec39/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RkhKbGMybDZaVjkwYjE5c2FXMXBkRnNIYVdscEFaWT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--c01fc0e39e2bc6b9e8e11f4e19b142c241ba9ba0/Lontine,%20Susan.jpg"></a></div>
      <span><div><a href="/legislators/42">Susan Lontine</a></div><div></div></span>
      </div>
      <div class="member large-4 col-md-6 col-lg-4 medium-6 columns cells">
      <div class="circular"><a href="/legislators/98"><img width="100" height="100" class="legislator-avatar" src="http://localhost:3000/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBadz09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--945c0146958a5d3c1a52880db8eb5526310a0dbf/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RkhKbGMybDZaVjkwYjE5c2FXMXBkRnNIYVdscEFaWT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--c01fc0e39e2bc6b9e8e11f4e19b142c241ba9ba0/Moreno-CO-17.jpg"></a></div>
      <span><div><a href="/legislators/98">Dominick Moreno</a></div><div></div></span>
      </div>
      </div>
      </div>
      <p><br></p>
      <div class="committee-meetings" id="meeting-accordion">
      <h2>Committee Meetings</h2>
      <div class="meeting" id="">
      <a data-toggle="collapse" data-target="#J_CBAC_2021A_20210507_133742" aria-expanded="false" aria-controls="collapseExample" class="">
      <h3 class="cwr_committee_meeting_dates">
      2021-05-07 (1)
      <i class="fa fa-chevron-up pull-right" aria-hidden="true"></i>
      </h3>
      </a>
      <div class="cwr_committee_meeting_uuids collapse show" id="J_CBAC_2021A_20210507_133742" data-parent="#meeting-accordion">
      <table class="cmhi table table-striped cwr_hearing_J_CBAC_2021A_20210507_133742">
      <tbody><tr>
      <th>Hearing Item</th>
      <th>Actions</th>
      <th>Documents/Links</th></tr>
      <tr data-hearing-item-uuid="6E73C18D95BF1037872586CE006BD71F" class="cwr_hearing_item_uuids" id="6E73C18D95BF1037872586CE006BD71F">
      <td data-label="Hearing Item" class="cwr_hearing_item_summary_title">Proposed General Rose Statue and Lincoln Park name change</td>
      <td data-label="Actions" class="cwr_hearing_item_bill_action">Amended, referred to the Committee of the Whole</td>
      <td data-label="Documents/Links" class="cmhi-docs">
      <a class="cwr_hearing_item_comm_report_url" href="https://s3.us-west-2.amazonaws.com/coga6.denvertech.org/xioj6rsnci46gr2vci8sr7pvmgfk?response-content-disposition=inline%3B%20filename%3D%22comm-report-D171BE9C57366D52872586CE0052AB3E.pdf%22%3B%20filename%2A%3DUTF-8%27%27comm-report-D171BE9C57366D52872586CE0052AB3E.pdf&amp;response-content-type=application%2Fpdf&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAIMZLVXQY6SSNVFJA%2F20210526%2Fus-west-2%2Fs3%2Faws4_request&amp;X-Amz-Date=20210526T185901Z&amp;X-Amz-Expires=300&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=6b4d07eaa6c540a28aba90253cca32f3beac8688a26b8cdff7350f1005b85ef9">Committee Report</a> | <a class="cwr_hearing_item_bill_summ_url" href="/committee_meeting_hearing_summary/24322">Hearing Summary</a> | <a class="cwr_hearing_item_vote_summaries_url" href="/hearing_item_votes/24322">Votes</a>
      </td>
      </tr>
      </tbody></table>
      </div>
      </div>
      <div class="meeting" id="">
      <a data-toggle="collapse" data-target="#J_CBAC_2021A_20210219_133555" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
      <h3 class="cwr_committee_meeting_dates">
      2021-02-19 (3)
      <i class="fa fa-chevron-up pull-right" aria-hidden="true"></i>
      </h3>
      </a>
      <div class="cwr_committee_meeting_uuids collapse " id="J_CBAC_2021A_20210219_133555" data-parent="#meeting-accordion">
      <table class="cmhi table table-striped cwr_hearing_J_CBAC_2021A_20210219_133555">
      <tbody><tr>
      <th>Hearing Item</th>
      <th>Actions</th>
      <th>Documents/Links</th>
      </tr><tr data-hearing-item-uuid="1CADF2211380B6FA8725868100713ECC" class="cwr_hearing_item_uuids" id="1CADF2211380B6FA8725868100713ECC">
      <td data-label="Hearing Item" class="cwr_hearing_item_summary_title">Visitor Services and Gift Shop Updates</td>
      <td data-label="Actions" class="cwr_hearing_item_bill_action"></td>
      <td data-label="Documents/Links" class="cmhi-docs">
      </td>
      </tr>
      <tr data-hearing-item-uuid="782A767710E6E90C87258681007452E0" class="cwr_hearing_item_uuids" id="782A767710E6E90C87258681007452E0">
      <td data-label="Hearing Item" class="cwr_hearing_item_summary_title">Update on Sand Creek Memorial Artwork</td>
      <td data-label="Actions" class="cwr_hearing_item_bill_action"></td>
      <td data-label="Documents/Links" class="cmhi-docs">
      </td>
      </tr>
      <tr data-hearing-item-uuid="8B62BC85CA9E9AEA8725868100757FC1" class="cwr_hearing_item_uuids" id="8B62BC85CA9E9AEA8725868100757FC1">
      <td data-label="Hearing Item" class="cwr_hearing_item_summary_title">Sub-basement Storage Room Project</td>
      <td data-label="Actions" class="cwr_hearing_item_bill_action"></td>
      <td data-label="Documents/Links" class="cmhi-docs">
      </td>
      </tr>
      </tbody></table>
      </div>
      </div>
      </div>
      </div>
      </div>
      <div class="footer-wrapper">
      <div class="footer">
      <ul class="flex-container">
      <li class="flex-item">
      <h4 class="block-title">Colorado General Assembly</h4>
      <div class="field-item even">
      <div>Colorado General Assembly</div>
      <div>200 E&nbsp;Colfax Avenue</div>
      <div>Denver, CO 80203</div>
      <p><a href="mailto:comments.ga@state.co.us" class="mailto">comments.ga@state.co.us<span class="mailto"><span class="element-invisible"> (link sends e-mail)</span></span></a></p>
      </div>
      </li>
      </ul>
      </div>
      </div>
      </body></html>
  ')
  end

  def html_page_no_data
    Nokogiri::HTML('
    <html lang="en"><head><style type="text/css" data-tag-name="trix-editor">trix-editor {
    display: block;
    }
    trix-editor:empty:not(:focus)::before {
    content: attr(placeholder);
    color: graytext;
    cursor: text;
    pointer-events: none;
    }
    trix-editor a[contenteditable=false] {
    cursor: text;
    }
    trix-editor img {
    max-width: 100%;
    height: auto;
    }
    trix-editor [data-trix-attachment] figcaption textarea {
    resize: none;
    }
    trix-editor [data-trix-attachment] figcaption textarea.trix-autoresize-clone {
    position: absolute;
    left: -9999px;
    max-height: 0px;
    }
    trix-editor [data-trix-attachment] figcaption[data-trix-placeholder]:empty::before {
    content: attr(data-trix-placeholder);
    color: graytext;
    }
    trix-editor [data-trix-cursor-target] {
    display: inline-block !important;
    width: 1px !important;
    padding: 0 !important;
    margin: 0 !important;
    border: none !important;
    }
    trix-editor [data-trix-cursor-target=left] {
    vertical-align: top !important;
    margin-left: -1px !important;
    }
    trix-editor [data-trix-cursor-target=right] {
    vertical-align: bottom !important;
    margin-right: -1px !important;
    }</style><style type="text/css" data-tag-name="trix-toolbar">trix-toolbar {
    display: block;
    }
    trix-toolbar {
    white-space: nowrap;
    }
    trix-toolbar [data-trix-dialog] {
    display: none;
    }
    trix-toolbar [data-trix-dialog][data-trix-active] {
    display: block;
    }
    trix-toolbar [data-trix-dialog] [data-trix-validate]:invalid {
    background-color: #ffdddd;
    }</style>
    <title>Pension Review Commission | Colorado General Assembly</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="csrf-param" content="authenticity_token">
    <meta name="csrf-token" content="W5pXWtb7uTacqL98Yz2rnVYigEZuXNHhh8pydUB53//byC+bl0Xa+11yotYcCMKqMKqh1Wytk6EhH0rn3AqDqg==">
    <link rel="stylesheet" media="screen" href="/assets/application.debug-24c376c72016760dcf2f5eee709620c4fcf278a7f7909c390613d06b97b46c66.css">
    <script src="/packs/js/application-737d6d1816b646521b6f.js"></script>
    </head>
    <body>
    <a class="skip-main" href="#main">Skip to main content</a>
    <div class="header-wrapper">
    <nav class="navbar navbar-light bg-light">
    <div class="container">
    <span class="nav-brand">
    <a class="navbar-brand" href="/">
    <span class="site_logo"><img src="/images/cga-logo-med.png" class="d-inline-block align-top" alt=""></span>
    <span class="site_title"><h1>Colorado General Assembly</h1></span>
    </a>
    </span>
    <span class="nav-search">
    <form class="form-inline my-2 my-lg-0" method="get" action="/search">
    <input class="form-control mr-sm-2" type="search" value="" name="q" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    </span>
    </div>
    </nav>
    <nav class="navbar navbar-expand-md navbar-light bg-light" id="navbarSupportedContent">
    <div class="container">
    <div class="" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Schedule
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="/session-schedule">Schedule</a>
    </div>
    </li>
    <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Bills
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="/bills">Bills</a>
    <a class="dropdown-item" href="/search?bills">Find a Bill</a>
    </div>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="">Laws</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="/legislators">Legislators</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="/committees">Committees</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="/initiatives">Initiatives</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="">Budget</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="">Audits</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="/publications">Publications</a>
    </li>
    <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Agencies
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="/agencies/house-of-representatives">House of Representatives</a>
    <a class="dropdown-item" href="/agencies/senate">Senate</a>
    <a class="dropdown-item" href="/agencies/office-of-the-state-auditor">Office of the State Auditor</a>
    <a class="dropdown-item" href="/agencies/office-of-legislative-legal-services">Office of Legislative Legal Services</a>
    <a class="dropdown-item" href="/agencies/legislative-council-staff">Legislative Council Staff</a>
    <a class="dropdown-item" href="/agencies/joint-budget-committee">Joint Budget Committee</a>
    <a class="dropdown-item" href="/agencies/joint-budget-committee">Joint Budget Committee Staff</a>
    </div>
    </li>
    </ul>
    </div>
    </div>
    </nav>
    </div>
    <div class="main-wrapper" id="main">
    <div class="page-wrapper">
    <p class="notice"></p>
    <p class="alert"></p>
    <div class="row grid-x">
    <div class="medium-8 cells columns col-md-8 committee-top">
    <h1>Pension Review Commission</h1>
    <div class="description">The commission studies and proposes legislation relating to funding of police officers and firefighters pensions in this state and benefit designs of such pension plans.  The commission also studies and proposes legislation regarding the Public Employees Retirement Association.</div>
    </div>
    <div class="medium-4 columns cells col-md-4 comm-desc">
    <p>
    <a href="/committee_audio/454">Committee Audio</a>
    </p>
    <p>
    <strong>Committee:</strong>
    <span id="cwr_clics_comm_id">
    I_PensionRevew_2021A
    </span>
    </p>
    <p>
    <strong>Chamber</strong>
    Interim Committee
    </p>
    <p>
    <strong>Committee Type:</strong>
    Interim Committee
    </p>
    <p>
    <strong>Session:</strong>
    2021 Regular Session
    </p>
    <p>
    <strong>Staff email:</strong>
    PensionReviewComm.ga@state.co.us
    </p>
    </div>
    <div class="row committee-details ">
    </div>
    </div>
    <div class="committee-members">
    <p class="medium-text-center committee-members-header">Committee Members</p>
    <div class="row grid-x">
    <div class="member large-4 col-md-6 col-lg-4 medium-6 columns cells">
    <div class="circular"><a href="/legislators/133"><img width="100" height="100" class="legislator-avatar" src="https://cogar.denvertech.org/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMld0QXc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--21a67a31a064d3c35b529f818ea920fb67b33b01/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RkhKbGMybDZaVjkwYjE5c2FXMXBkRnNIYVdscEFaWT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--1fdcf030234b36a1e6308b26d6d18947293689f1/Bird,%20Shannon.jpg"></a></div>
    <span><div><a href="/legislators/133">Shannon Bird</a></div><div></div></span>
    </div>
    <div class="member large-4 col-md-6 col-lg-4 medium-6 columns cells">
    <div class="circular"><a href="/legislators/151"><img width="100" height="100" class="legislator-avatar" src="https://cogar.denvertech.org/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBNG10QXc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--2e27bcbfc6f2ec0f313125721eac99d7e1c976e9/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RkhKbGMybDZaVjkwYjE5c2FXMXBkRnNIYVdscEFaWT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--1fdcf030234b36a1e6308b26d6d18947293689f1/Sirota,%20Emily.jpg"></a></div>
    <span><div><a href="/legislators/151">Emily Sirota</a></div><div></div></span>
    </div>
    </div>
    </div>
    <p><br></p>
    <div class="committee-meetings" id="meeting-accordion">
    <h2>Committee Meetings</h2>
    <span class="cwr_committee_meeting_dates cwr_committee_meeting_uuids">No committee meetings available.</span>
    </div>
    </div>
    </div>
    <div class="footer-wrapper">
    <div class="footer">
    <ul class="flex-container">
    <li class="flex-item">
    <h4 class="block-title">Colorado General Assembly</h4>
    <div class="field-item even">
    <div>Colorado General Assembly</div>
    <div>200 E&nbsp;Colfax Avenue</div>
    <div>Denver, CO 80203</div>
    <p><a href="mailto:comments.ga@state.co.us" class="mailto">comments.ga@state.co.us<span class="mailto"><span class="element-invisible"> (link sends e-mail)</span></span></a></p>
    </div>
    </li>
    <li class="flex-item">
    Resources &amp; Information
    </li>
    <li class="flex-item">
    Policies
    </li>
    <li class="flex-item">
    For Legislators &amp; Staff
    <div class="columns ">
    <p class="mt-2">
    <a class="btn btn-success" href="/users/sign_in">Login</a>
    </p>
    </div>
    </li>
    </ul>
    </div>
    </div>
    </body></html>
    ')
  end
end

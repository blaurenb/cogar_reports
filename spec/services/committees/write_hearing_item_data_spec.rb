# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/committee_data'

RSpec.describe Committees::WriteHearingItemData do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:cm) { CommitteeData.new }
    let!(:committee) { Committee.create!(clics_lm_digest: 'comm123', clics_committee_id: 'COMM_123_2016A') }
    let!(:hearing) { Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: '123abc') }
    let!(:writer_struct) do
      OpenStruct.new(report: report,
                     committee: committee,
                     hearing: hearing,
                     h_data: nil)
    end

    it 'performs call' do
      h_data = cm.clics_committee_mtg_data_triple_hi
      expect(HearingItem.all.length).to eq(0)
      writer_struct.h_data = h_data
      Committees::WriteHearingItemData.new(writer_struct).call

      expect(HearingItem.all.length).to eq(3)
      expect(HearingItem.first.hearing_id).to eq(hearing.id)
      expect(HearingItem.second.hearing_id).to eq(hearing.id)
      expect(HearingItem.third.hearing_id).to eq(hearing.id)
      expect(HearingItem.first.hearing_item_uuid).to eq('0E0D0D64DAF593A68725867900716A28')
      expect(HearingItem.second.hearing_item_uuid).to eq('A88B48BA58FBDC578725867900751199')
      expect(HearingItem.third.hearing_item_uuid).to eq('94F3A87005F0A7AE8725867900777819')
    end

    it 'performs do_hearing_items' do
      h_data = cm.clics_committee_mtg_data_triple_hi
      expect(HearingItem.all.length).to eq(0)
      writer_struct.h_data = h_data
      whid = Committees::WriteHearingItemData.new(writer_struct)
      whid.do_hearing_items

      expect(HearingItem.all.length).to eq(3)
      expect(HearingItem.first.hearing_id).to eq(hearing.id)
      expect(HearingItem.second.hearing_id).to eq(hearing.id)
      expect(HearingItem.third.hearing_id).to eq(hearing.id)
      expect(HearingItem.first.hearing_item_uuid).to eq('0E0D0D64DAF593A68725867900716A28')
      expect(HearingItem.second.hearing_item_uuid).to eq('A88B48BA58FBDC578725867900751199')
      expect(HearingItem.third.hearing_item_uuid).to eq('94F3A87005F0A7AE8725867900777819')
    end

    it 'performs create_or_update_hearing_items(b_data) with good data' do
      b_data = JSON.parse('{"bill_num":"bill_num123","summary_title":"summary_title123","bill_action":"bill_action123","hearing_item_uuid":"hearing_item_uuid123","bill_summ_url":"bill_summ_url123","granicus_url":"granicus_url123","event_url":"event_url123","comm_report_url":["comm_report_url123"],"attachments":[],"vote_summaries":[{"url":"vote_summaries_url123"}]}')

      expect(HearingItem.all.length).to eq(0)
      # writer_struct.h_data = h_data
      whid = Committees::WriteHearingItemData.new(writer_struct)
      whid.create_or_update_hearing_items(b_data)

      expect(HearingItem.all.length).to eq(1)
      expect(HearingItem.last.hearing_id).to eq(hearing.id)
      expect(HearingItem.last.hearing_item_uuid).to eq('hearing_item_uuid123')
      expect(HearingItem.last.bill_num).to eq('bill_num123')
      expect(HearingItem.last.summary_title).to eq('summary_title123')
      expect(HearingItem.last.bill_action).to eq('bill_action123')
      expect(HearingItem.last.bill_summ_url).to eq('bill_summ_url123')
      expect(HearingItem.last.comm_report_url).to eq('comm_report_url123')
      expect(HearingItem.last.vote_summaries_url).to eq('vote_summaries_url123')
    end

    it 'performs create_or_update_hearing_items(b_data) with bad data' do
      b_data = JSON.parse('{"bill_num":"bill_num123","summary_title":"summary_title123","bill_action":"bill_action123","hearing_item_uuid":"","bill_summ_url":"bill_summ_url123","granicus_url":"granicus_url123","event_url":"event_url123","comm_report_url":["comm_report_url123"],"attachments":[],"vote_summaries":[{"url":"vote_summaries_url123"}]}')

      expect(HearingItem.all.length).to eq(0)
      # writer_struct.h_data = h_data
      whid = Committees::WriteHearingItemData.new(writer_struct)
      whid.create_or_update_hearing_items(b_data)
      expect(HearingItem.all.length).to eq(0)
    end
  end
end

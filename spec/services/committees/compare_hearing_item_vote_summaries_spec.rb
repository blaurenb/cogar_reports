# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/nokogiri_vote_summary_page_data'

RSpec.describe Committees::Comparisons::CompareHearingItems do
  describe 'Methods' do
    let!(:report) { ::Reports::CommitteeReport.create! }
    let!(:committee) { Committee.create!(clics_committee_id: 'J_CBAC_2021A', clics_lm_digest: '123abc') }
    let!(:hearing) do
      Hearing.create!(clics_m_id_digest: 'bcd123', meeting_uid: 'J_CBAC_2021A_20210507_133742',
                      hearing_item_uuids: ['6E73C18D95BF1037872586CE006BD71F'])
    end
    let!(:committee_hearing) { CommitteeHearing.create!(committee_id: committee.id, hearing_id: hearing.id) }

    let!(:hearing_item) do
      HearingItem.create!(hearing_id: hearing.id,
                          hearing_item_uuid: '6E73C18D95BF1037872586CE006BD71F',
                          bill_num: 'bill_num',
                          summary_title: 'Proposed General Rose Statue and Lincoln Park name change',
                          bill_action: 'Amended, referred to the Committee of the Whole',
                          bill_summ_url: 'www.bill_summ_url.com',
                          comm_report_url: 'www.comm_report_url.com',
                          vote_summaries_url: 'www.vote_summaries.com',
                          votes: [{ 'vote_time' => '02/25/2021 10:44:20 AM' },
                                  { 'vote_time' => '02/25/2021 10:45:09 AM' },
                                  { 'vote_time' => '02/25/2021 10:58:31 AM' }])
    end
    let!(:vote_summary1) do
      VoteSummary.create!(hearing_item_id: hearing_item.id,
                          clics_vote_digest: 'test1',
                          url: 'test',
                          vote_time: '02/25/2021 10:44:20 AM',
                          vote_motion: 'Adopt amendment L.003 (Attachment A).')
    end
    let!(:vote_summary2) do
      VoteSummary.create!(hearing_item_id: hearing_item.id,
                          clics_vote_digest: 'test2',
                          url: 'test',
                          vote_time: '02/25/2021 10:45:09 AM',
                          vote_motion: 'Adopt amendment L.005 (Attachment B).')
    end
    let!(:vote_summary3) do
      VoteSummary.create!(hearing_item_id: hearing_item.id,
                          clics_vote_digest: 'test3',
                          url: 'test',
                          vote_time: '02/25/2021 10:58:31 AM',
                          vote_motion: 'Refer House Bill 21-1102, as amended, to the Committee of the Whole.')
    end
    let!(:tp) do
      TargetPage.create!(parentable_id: HearingItem.first.id, parentable_type: 'HearingItem', url_filepath: 'url_filepath',
                         target_slug: 'target_slug')
    end
    let!(:nd) { NokogiriVoteSummaryPageData.new }
    let!(:comparables) do
      OpenStruct.new(report: report,
                     tp: tp,
                     doc: nd.html_page)
    end
    let!(:chivs) { Committees::Comparisons::CompareHearingItemVoteSummaries.new(comparables) }
    let!(:ei_struct) do
      OpenStruct
        .new(report: comparables.report,
             tp: comparables.tp,
             clics_key: '',
             clics_value: '',
             target_css: '',
             target_value: '',
             sort_error: 0)
    end

    it 'call' do
      expect(ErrorItem.all.length).to eq(0)
      chivs.call
      expect(ErrorItem.all.length).to eq(7)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(VoteSummaryErrorItem.all.length).to eq(6)
    end

    it 'compare_vote_summary_order with good data' do
      expect(ErrorItem.all.length).to eq(0)
      chivs.compare_vote_summary_order

      expect(ErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingItemErrorItem.last.hearing_item_id).to eq(hearing_item.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('vote summary order: vote_summaries.vote_time')
      expect(ErrorItem.last.clics_value).to eq('["02/25/2021 10:44 AM", "02/25/2021 10:45 AM", "02/25/2021 10:58 AM"]')
      expect(ErrorItem.last.target_css).to eq('.cwr_vote_item')
      expect(ErrorItem.last.target_value).to eq('["02/25/2021 10:44 AM", "02/25/2021 10:45 AM", "02/25/2021 10:58 AM"]')
    end

    it 'compare_vote_summary_order with bad data' do
      hearing_item.update(votes: [{ 'vote_time' => '02/25/2021 08:44:20 AM' },
                                  { 'vote_time' => '02/25/2021 09:45:09 AM' },
                                  { 'vote_time' => '02/25/2021 10:58:31 AM' }])
      expect(ErrorItem.all.length).to eq(0)
      chivs.compare_vote_summary_order

      expect(ErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.all.length).to eq(1)
      expect(HearingItemErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(HearingItemErrorItem.last.hearing_item_id).to eq(hearing_item.id)
      expect(ErrorItem.last.sort_error).to eq('coga incorrect order or missing values')
      expect(ErrorItem.last.clics_key).to eq('vote summary order: vote_summaries.vote_time')
      expect(ErrorItem.last.clics_value).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(ErrorItem.last.target_css).to eq('.cwr_vote_item')
      expect(ErrorItem.last.target_value).to eq('["02/25/2021 10:44 AM", "02/25/2021 10:45 AM", "02/25/2021 10:58 AM"]')
    end

    it 'get_clics_vote_times' do
      vote_times = chivs.get_clics_vote_times
      expect(vote_times).to eq(['02/25/2021 10:44 AM', '02/25/2021 10:45 AM', '02/25/2021 10:58 AM'])
    end

    it 'get_tp_vote_times' do
      vote_times = chivs.get_tp_vote_times
      expect(vote_times).to eq(['02/25/2021 10:44 AM', '02/25/2021 10:45 AM', '02/25/2021 10:58 AM'])
    end

    it 'compare_fields_values(hi_field, vote_summary_obj) with good data' do
      expect(ErrorItem.all.length).to eq(0)
      chivs.compare_fields_values('vote_motion', vote_summary2)

      expect(ErrorItem.all.length).to eq(1)
      expect(VoteSummaryErrorItem.all.length).to eq(1)
      expect(VoteSummaryErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(VoteSummaryErrorItem.last.vote_summary_id).to eq(vote_summary2.id)
      expect(ErrorItem.last.sort_error).to eq('ok')
      expect(ErrorItem.last.clics_key).to eq('vote_summaries.vote_motion')
      expect(ErrorItem.last.clics_value).to eq(vote_summary2.vote_motion)
      expect(ErrorItem.last.target_css).to eq('.cwr_vote_motion')
      expect(ErrorItem.last.target_value).to eq(vote_summary2.vote_motion)
    end

    it 'compare_fields_values(hi_field, vote_summary_obj) with bad data' do
      expect(ErrorItem.all.length).to eq(0)
      motion = vote_summary2.vote_motion
      vote_summary2.update!(vote_motion: nil)
      chivs.compare_fields_values('vote_motion', vote_summary2)

      expect(ErrorItem.all.length).to eq(1)
      expect(VoteSummaryErrorItem.all.length).to eq(1)
      expect(VoteSummaryErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
      expect(VoteSummaryErrorItem.last.vote_summary_id).to eq(vote_summary2.id)
      expect(ErrorItem.last.sort_error).to eq('clics empty value')
      expect(ErrorItem.last.clics_key).to eq('vote_summaries.vote_motion')
      expect(ErrorItem.last.clics_value).to eq(nil)
      expect(ErrorItem.last.target_css).to eq('.cwr_vote_motion')
      expect(ErrorItem.last.target_value).to eq(motion)
    end

    it 'get_target_value(doc, hearing_item, hi_field)' do
      target_val = chivs.get_target_value(vote_summary1, 'vote_motion_header')
      expect(target_val).to eq(vote_summary1.vote_motion)

      target_val = chivs.get_target_value(vote_summary1, 'vote_motion')
      expect(target_val).to eq(vote_summary1.vote_motion)
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Filters::RansackEarlyErrors do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:orig_params) do
      { early_sort_errors: ['', ''], comparison_sort_errors: ['', ''], sessions: ['', ''], committee_origins: ['', ''],
        committee_ids: ['', ''] }
    end
    let!(:c1) do
      Committee.create!(clics_lm_digest: 'digest for MY_COMM_1_2021A', clics_committee_id: 'H_COMM_1_2021A')
    end
    let!(:ee1) do
      EarlyError.create!(reportable_id: report.id,
                         reportable_type: report.type,
                         description: "Parsing error for endpoint: http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/#{c1.clics_committee_id}.",
                         sort_error: 2)
    end
    let!(:c2) do
      Committee.create!(clics_lm_digest: 'digest for MY_COMM_2_2020A', clics_committee_id: 'S_COMM_2_2020A')
    end
    let!(:ee2) do
      EarlyError.create!(reportable_id: report.id,
                         reportable_type: report.type,
                         description: 'nil Good',
                         sort_error: 0)
    end
    let!(:c3) do
      Committee.create!(clics_lm_digest: 'digest for MY_COMM_3_2019A', clics_committee_id: 'I_COMM_3_2019A')
    end
    let!(:ee3) do
      EarlyError.create!(reportable_id: report.id,
                         reportable_type: report.type,
                         description: "Parsing error for endpoint: http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/#{c3.clics_committee_id}.",
                         sort_error: 2)
    end

    it 'q_sort_errors' do
      params = {}.merge(orig_params)

      params[:early_sort_errors] = ['', '0']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sort_errors
      expect(result).to eq([0])
      result = ree.search
      expect(result.length).to eq(1)

      params[:early_sort_errors] = ['', '2']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sort_errors
      expect(result).to eq([2])
      result = ree.search
      expect(result.length).to eq(2)

      params[:early_sort_errors] = ['', '2', '0']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sort_errors
      expect(result).to eq([2, 0])
      result = ree.search
      expect(result.length).to eq(3)

      params[:early_sort_errors] = ['', '999']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sort_errors
      expect(result).to eq([999])
      result = ree.search
      expect(result.length).to eq(0)

      params[:early_sort_errors] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sort_errors
      expect(result).to eq(nil)
      result = ree.search
      expect(result.length).to eq(3)

      params[:early_sort_errors] = ['', 'nil']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sort_errors
      expect(result).to eq(nil)
      result = ree.search
      expect(result.length).to eq(3)
    end

    it 'q_sessions' do
      params = {}.merge(orig_params)

      params[:sessions] = ['', '2021a']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sessions
      expect(result).to eq(['2021a'])
      result = ree.call
      expect(result.length).to eq(1)

      params[:sessions] = ['', '2021a', '2019a']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sessions
      expect(result).to eq(%w[2021a 2019a])
      result = ree.call
      expect(result.length).to eq(2)

      # - - with bad or missing data
      params[:sessions] = ['', '2021a', '2020a']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sessions
      expect(result).to eq(%w[2021a 2020a])
      result = ree.call
      expect(result.length).to eq(1)

      params[:sessions] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sessions
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)

      params[:sessions] = ['', 'nil']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sessions
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)

      params[:sessions] = ['', '2999a']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_sessions
      expect(result).to eq(['2999a'])
      result = ree.call
      expect(result.length).to eq(0)
    end

    it 'q_committee_origins' do
      params = {}.merge(orig_params)

      params[:committee_origins] = ['', 'house']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_origins
      expect(result).to eq(['H_'])
      result = ree.call
      expect(result.length).to eq(1)

      params[:committee_origins] = ['', 'house', 'joint']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_origins
      expect(result).to eq(%w[H_ I_ J_ O_ SE_])
      result = ree.call
      expect(result.length).to eq(2)

      # - - with bad or missing data
      params[:committee_origins] = ['', 'house', 'bogus']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_origins
      expect(result).to eq(%w[H_])
      result = ree.call
      expect(result.length).to eq(1)

      params[:committee_origins] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_origins
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)

      params[:committee_origins] = ['', 'nil']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_origins
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)

      params[:committee_origins] = ['', 'bogus']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_origins
      expect(result).to eq([])
      result = ree.call
      expect(result.length).to eq(3)
    end

    it 'q_committee_ids' do
      params = {}.merge(orig_params)

      params[:committee_ids] = ['', c1.id.to_s]
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_ids(params[:committee_ids])
      expect(result).to eq([c1.clics_committee_id.to_s])
      result = ree.call
      expect(result.length).to eq(1)

      params[:committee_ids] = ['', c1.id.to_s, c3.id.to_s]
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_ids(params[:committee_ids])
      expect(result).to eq([c1.clics_committee_id.to_s, c3.clics_committee_id.to_s])
      result = ree.call
      expect(result.length).to eq(2)

      # - - with bad or missing data
      params[:committee_ids] = ['', c1.id.to_s, c2.id.to_s]
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_ids(params[:committee_ids])
      expect(result).to eq([c1.clics_committee_id.to_s, c2.clics_committee_id.to_s])
      result = ree.call
      expect(result.length).to eq(1)

      params[:committee_ids] = ['', '999']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_ids(params[:committee_ids])
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)

      params[:committee_ids] = ['', 'none']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_ids(params[:committee_ids])
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)

      params[:committee_ids] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_ids(params[:committee_ids])
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)

      params[:committee_ids] = ['', 'nil']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.q_committee_ids(params[:committee_ids])
      expect(result).to eq(nil)
      result = ree.call
      expect(result.length).to eq(3)
    end

    it 'returns the right data from .call()' do
      params = {}.merge(orig_params)

      params[:early_sort_errors] = ['', '']
      params[:sessions] = ['', '']
      params[:committee_ids] = ['', '']
      params[:committee_origins] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(3)

      params[:early_sort_errors] = ['', 'nil']
      params[:sessions] = ['', 'nil']
      params[:committee_ids] = ['', 'nil']
      params[:committee_origins] = ['', 'nil']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(3)

      params[:early_sort_errors] = ['', 'none']
      params[:sessions] = ['', 'nil']
      params[:committee_ids] = ['', '']
      params[:committee_origins] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(0)

      params[:early_sort_errors] = ['', '']
      params[:sessions] = ['', 'none']
      params[:committee_ids] = ['', 'none']
      params[:committee_origins] = ['', 'none']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(3)

      params[:early_sort_errors] = ['', '0']
      params[:sessions] = ['', '']
      params[:committee_ids] = ['', 'nil']
      params[:committee_origins] = ['', 'nil']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(1)

      params[:early_sort_errors] = ['', '0']
      params[:sessions] = ['', '']
      params[:committee_ids] = ['', '']
      params[:committee_origins] = ['', 'house']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(0)

      params[:early_sort_errors] = ['', '0']
      params[:sessions] = ['', '']
      params[:committee_ids] = ['', 'nil']
      params[:committee_origins] = ['', 'house']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(0)

      params[:early_sort_errors] = ['', '2']
      params[:sessions] = ['', '']
      params[:committee_ids] = ['', c3.id.to_s]
      params[:committee_origins] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(1)

      params[:early_sort_errors] = ['', '0']
      params[:sessions] = ['', '']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:committee_origins] = ['', 'house']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(0)

      params[:early_sort_errors] = ['', '0', '2']
      params[:sessions] = ['', '']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:committee_origins] = ['', 'house']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(1)

      params[:early_sort_errors] = ['', '0', '2']
      params[:sessions] = ['', '']
      params[:committee_origins] = ['', '']
      params[:committee_ids] = ['', c1.id.to_s, c3.id.to_s]
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(2)
      expect(result[0].sort_error).to eq('clics: json error')
      expect(result[1].sort_error).to eq('clics: json error')

      params[:early_sort_errors] = ['', '']
      params[:sessions] = ['', '2021a']
      params[:committee_origins] = ['', 'house']
      params[:committee_ids] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(1)
      expect(result[0].sort_error).to eq('clics: json error')

      params[:early_sort_errors] = ['', '']
      params[:sessions] = ['', '2019a']
      params[:committee_origins] = ['', 'house']
      params[:committee_ids] = ['', '']
      ree = Filters::RansackEarlyErrors.new(params, report.id)
      result = ree.call
      expect(result.length).to eq(0)
    end
  end
end

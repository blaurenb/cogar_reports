# frozen_string_literal: true

class KickOffJob < ApplicationJob
  queue_as :default

  def perform
    logger.info "Kicking Off Reports on #{DateTime.now.strftime('%m-%d-%Y at%H:%M:%S')}"

    CommitteeReportJob.perform_now
    # BillsReportJob.perform
    # ScheduleReportJob.perform
  end
end

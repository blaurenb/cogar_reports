# frozen_string_literal: true

class CommitteeReportJob < ApplicationJob
  queue_as :default

  def perform
    logger.info 'Starting CommitteeReportJob'
    report = Reports::CommitteeReport.create
    # json_getter = GetJson.new
    # scraper = Scraper.new

    c_data = Committees::GetClicsCommitteeData.new(report).call
    logger.info 'GetClicsCommitteeData complete'

    Committees::WriteCommitteesData.new(report, c_data).call if c_data.present?
    logger.info 'WriteCommitteesData complete'

    Committees::GetClicsHearingData.new(report).call
    logger.info 'GetClicsHearingData complete.'

    Committees::CommitteeTargetPages.new(report).call
    logger.info 'CommitteeTargetPages complete'

    Committees::ScrapeCommitteePages.new(report)
    logger.info 'ScrapeCommitteePages complete'

    Committees::HearingItemTargetPages.new(report).call
    logger.info 'HearingItemTargetPages complete'

    Committees::ScrapeHearingItemVoteSummaryPages.new(report).call
    logger.info 'ScrapeHearingItemVoteSummaryPages complete'

    DigestifyErrors.new(report)
    logger.info 'DigestifyErrors complete'

    logger.info "CommitteeReportJob completed. Report id : #{report.id} "
  end
end

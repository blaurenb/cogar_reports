# frozen_string_literal: true

module Reports
  class DownloadsController < ApplicationController
    def download_committee_ei
      @report = Reports::Report.find(params[:report_id])
      @csv = Reports::CommitteePagesCsvService.new(@report).call
      handle_download
    end

    def download_committee_ee
      @report = Reports::Report.find(params[:report_id])
      @csv = Reports::CommitteeEarlyErrorCsvService.new(@report).call
      handle_download
    end

    def handle_download
      if @csv.error == true
        msg = 'We hit a snag  writing the CSV report. See logs for details. Error Message: '
        redirect_to report_path(id: @report.id), alert: msg and return
      end

      respond_to do |format|
        format.csv do
          send_file(@csv.file_path,
                    filename: File.basename(@csv.file_name),
                    type: 'text/csv',
                    disposition: 'attachment')
        rescue StandardError => e
          msg = "We hit a snag  writing the CSV report. Error: #{e}"
          redirect_to report_path(report_id: @report.id, format: :html), alert: msg and return
        end
      end
    end
  end
end

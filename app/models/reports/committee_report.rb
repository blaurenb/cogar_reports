# frozen_string_literal: true

# == Schema Information
#
# Table name: reports
#
#  id         :bigint           not null, primary key
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
module Reports
  class CommitteeReport < Report
    attribute :type, :string, default: 'Reports::CommitteeReport'

    def early_errors
      EarlyError.where(reportable_id: id)
    end

    def error_items
      ErrorItem.where(reportable_id: id)
    end
  end
end

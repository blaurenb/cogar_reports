# frozen_string_literal: true

# == Schema Information
#
# Table name: reports
#
#  id         :bigint           not null, primary key
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
module Reports
  class Report < ApplicationRecord
    def error_items
      ErrorItem.where(reportable_id: id)
    end
  end
end

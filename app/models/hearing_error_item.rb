# frozen_string_literal: true

# == Schema Information
#
# Table name: hearing_error_items
#
#  id            :bigint           not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  error_item_id :bigint
#  hearing_id    :bigint
#
# Indexes
#
#  index_hearing_error_items_on_error_item_id  (error_item_id)
#  index_hearing_error_items_on_hearing_id     (hearing_id)
#
# Foreign Keys
#
#  fk_rails_...  (error_item_id => error_items.id)
#  fk_rails_...  (hearing_id => hearings.id)
#
class HearingErrorItem < ApplicationRecord
  belongs_to :error_item
  belongs_to :hearing

  def committees
    Committee.includes(:committee_hearings, :hearings).where(hearings: { id: hearing_id })
  end
end

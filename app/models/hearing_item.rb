# frozen_string_literal: true

# == Schema Information
#
# Table name: hearing_items
#
#  id                 :bigint           not null, primary key
#  bill_action        :string
#  bill_num           :string
#  bill_summ_url      :string
#  comm_report_url    :string
#  hearing_item_uuid  :string
#  summary_title      :string
#  vote_summaries_url :string
#  votes              :jsonb
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  hearing_id         :bigint
#
# Indexes
#
#  index_hearing_items_on_hearing_id  (hearing_id)
#
# Foreign Keys
#
#  fk_rails_...  (hearing_id => hearings.id)
#
class HearingItem < ApplicationRecord
  belongs_to :hearing
  has_many :committees, through: :hearing
  has_many :hearing_item_error_items
  has_many :error_items, through: :hearing_item_error_items
  has_many :vote_summaries

  validates :hearing_id, :hearing_item_uuid, presence: true
end

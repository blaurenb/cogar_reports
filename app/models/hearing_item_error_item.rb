# frozen_string_literal: true

# == Schema Information
#
# Table name: hearing_item_error_items
#
#  id              :bigint           not null, primary key
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  error_item_id   :bigint
#  hearing_item_id :bigint
#
# Indexes
#
#  index_hearing_item_error_items_on_error_item_id    (error_item_id)
#  index_hearing_item_error_items_on_hearing_item_id  (hearing_item_id)
#
# Foreign Keys
#
#  fk_rails_...  (error_item_id => error_items.id)
#  fk_rails_...  (hearing_item_id => hearing_items.id)
#
class HearingItemErrorItem < ApplicationRecord
  belongs_to :error_item
  belongs_to :hearing_item

  has_one :hearing, through: :hearing_item
  has_one :committee, through: :hearing

  def committees
    Committee.includes(:committee_hearings, :hearings, :hearing_items).where(hearing_items: { id: hearing_item_id })
  end
end

# frozen_string_literal: true

module Committees
  module Comparisons
    class CompareCommittees
      include Shared

      def initialize(comparables)
        @comparables = comparables
        @ei_struct = OpenStruct
                     .new(report: comparables.report,
                          tp: comparables.tp,
                          clics_key: '',
                          clics_value: '',
                          target_css: '',
                          target_value: '',
                          sort_error: 0)
      end

      def call
        compare_clics_comm_id(@comparables, @ei_struct)
        compare_meeting_dates(@comparables, @ei_struct)
      end

      def compare_clics_comm_id(comparables, ei_struct)
        doc = comparables.doc
        tp = comparables.tp

        ei_struct.clics_key = 'committee_name'
        ei_struct.clics_value = tp.parentable.clics_committee_id
        ei_struct.target_css = '#cwr_clics_comm_id'
        ei_struct.target_value = doc.css(ei_struct.target_css).text.strip
        ei_struct.sort_error = get_sort_error(ei_struct.clics_value, ei_struct.target_value)

        ei = create_error_item(ei_struct)
        CommitteeErrorItem
          .create!(committee_id: tp.parentable_id, error_item_id: ei.id)
      end

      def compare_meeting_dates(comparables, ei_struct)
        # Data per Hearing is currently limited to checking whether dates appear
        # on a committee page. Currently this function should be related to a
        # CommitteeErroItem object rather than a Hearing object.
        committee_id = comparables.tp.parentable_id
        target_css = '.cwr_committee_meeting_dates'
        target_value = get_target_value(comparables.doc, target_css)

        ei = set_up_ei_for_count(ei_struct, 'meeting_dates', target_value)
        CommitteeErrorItem.create!(committee_id: committee_id, error_item_id: ei.id)

        ei = set_up_ei_for_ordered_values(ei_struct, 'meeting_dates', target_value)
        CommitteeErrorItem.create!(committee_id: committee_id, error_item_id: ei.id)

        nil
      end

      def set_up_ei_for_count(ei_struct, committee_field, target_value)
        ei_struct.clics_key = "#{committee_field} count"
        ei_struct.clics_value = get_total_clics(ei_struct.tp.parentable.send(committee_field))
        ei_struct.target_css = ".cwr_committee_#{committee_field}"
        ei_struct.target_value = target_value.length
        ei_struct.sort_error = get_sort_error(ei_struct.clics_value, ei_struct.target_value)

        create_error_item(ei_struct)
      end

      def set_up_ei_for_ordered_values(ei_struct, committee_field, target_value)
        tp = ei_struct.tp
        ei_struct.clics_key = "#{committee_field} order"

        # clics dates are ordered oldest to newest on the committee object, but
        # the cogarwebsite needs to display them from most-recent to oldest.
        # Therefore reverse the order when comparing.
        ei_struct.clics_value = tp.parentable.send(committee_field).reverse!
        temp_clics_dates = get_reversed_clics_dates(ei_struct.clics_value)

        ei_struct.target_css = ".cwr_committee_#{committee_field}"
        ei_struct.target_value = target_value

        ei_struct.sort_error = get_ordered_sort_error(ei_struct.target_value, temp_clics_dates)

        create_error_item(ei_struct)
      end

      def get_reversed_clics_dates(reversed_clics_dates)
        reversed_clics_dates.map do |d|
          DateTime.parse(d).strftime('%Y-%m-%d')
        rescue StandardError
          d
        end
      end

      def get_total_clics(tp_field)
        tp_field_is_array = (tp_field.is_a?(Array) && tp_field != ['0'])
        tp_field_is_array ? tp_field.length : 0
      end

      def get_target_value(doc, target_css)
        target_value = doc.css(target_css)
        if target_value.text.strip == 'No committee meetings available.'
          []
        else
          target_value.map do |val|
            # split the string and select only the 10 digit dates (yyyy-mm-dd)
            val.text.strip[0..9]
          end
        end
      end
    end
  end
end

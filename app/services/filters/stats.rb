# frozen_string_literal: true

module Filters
  class Stats
    attr_reader :stats, :early_errors, :error_items

    def initialize(report, params)
      @stats = {}
      @early_errors = []
      @error_items = []
      call(report, params)
    end

    def call(report, params)
      load_errors(params, report)
      load_stats
    end

    def load_errors(params, report)
      if params.present? && params[:day_diff].present?
        errors_by_day(params, report)
      else
        errors_by_other_filters(params, report)
      end
    end

    def errors_by_day(params, report)
      day_diff_errors = ::Filters::DayDiffErrors.new(params[:day_diff], report, 'early_errors')
      @early_errors = day_diff_errors.errs

      day_diff_errors = ::Filters::DayDiffErrors.new(params[:day_diff], report, 'error_items')
      @error_items = day_diff_errors.errs
    end

    def errors_by_other_filters(params, report)
      @early_errors = ::Filters::RansackEarlyErrors.new(params, report.id).call
      @error_items = ::Filters::RansackErrorItems.new(params, report.id).call
    end

    def load_stats
      @stats[:stats_total_fields] = @error_items.length + @early_errors.length
      @stats[:stats_total_ee_errors] = @early_errors.length
      @stats[:stats_total_ei_errors] = qty_ei_not_in([0])
      @stats[:stats_total_errors_no_0_5] = qty_ei_not_in([0, 5])
      @stats[:stats_total_errors1] = qty_ee(1)
      @stats[:stats_total_errors2] = qty_ee(2)
      @stats[:stats_total_errors3] = qty_ei(3)
      @stats[:stats_total_errors4] = qty_ei(4)
      @stats[:stats_total_errors5] = qty_ei(5)
      @stats[:stats_total_errors6] = qty_ei(6)
      @stats[:stats_total_errors7] = qty_ei(7)
      @stats[:stats_total_errors8] = qty_ei(8)
      @stats[:stats_total_errors9] = qty_ei(9)
      @stats[:stats_total_errors10] = qty_ei(10)
    end

    def qty_ei_not_in(nums)
      @error_items.empty? ? 0 : @error_items.where.not(sort_error: nums).length
    end

    def qty_ee(n)
      @early_errors.empty? ? 0 : @early_errors.where(sort_error: n).length
    end

    def qty_ei(n)
      @error_items.empty? ? 0 : @error_items.where(sort_error: n).length
    end
  end
end

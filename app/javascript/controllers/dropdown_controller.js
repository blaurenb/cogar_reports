import { Controller } from "stimulus"

export default class DropdownController extends Controller {
  static targets = ["sessionsIds", "originIds", 'commIds']

  connect() {
    // in some cases for filtering,there are already selections made when the page loads. Since there are no click events but we want to trigger the filters to run, call it here on controller connect!
    this.updateCommIdList();
  }

  updateCommIdList() {
    // This works (this.data.get("comidlist") and ), but note that the capitals
    // have been dropped from what is listed in the html element.
    let filteredCommIds = JSON.parse(this.data.get("comidlist"))
    filteredCommIds = this.getFilteredOpts(filteredCommIds)

    this.commIdsTarget.length = 0

    var newOpts = filteredCommIds.map(item => new Option(item[0], item[1]))
    newOpts.forEach(c => this.commIdsTarget.appendChild(c))
    if (this.commIdsTarget.options[0].text != "All"){
      this.commIdsTarget.prepend(new Option("All", 'nil'))
    }

    this.setSelectedIdIfAny()
  }

  getFilteredOpts(filteredCommIds) {
    var seshFilters = this.getSeshFilters()
    var originFilters = this.getOriginFilters()

    filteredCommIds = this.filterBySesh(filteredCommIds, seshFilters)
    filteredCommIds = this.filterByOrgin(filteredCommIds, originFilters)

    if (seshFilters.length != 0 && originFilters.length != 0 ){
      filteredCommIds.shift()
    }
    return filteredCommIds
  }

  getSeshFilters() {
    var seshFilters = [...this.sessionsIdsTarget.selectedOptions]
    seshFilters = seshFilters.map( o => o.value )
    return seshFilters.filter(val => val != 'nil')
  }

  getOriginFilters() {
    var origins = {'senate': 'S_',
      'house': 'H_',
      'conference': 'CC_',
      'joint': ['I_', 'J_', 'O_', 'SE_'],
      }

    var originFilters = [...this.originIdsTarget.selectedOptions]

    originFilters = originFilters.map( o => o.value )
    originFilters = originFilters.filter(val => val != 'nil')
    return originFilters.map( elem => origins[elem]).flat(1)
  }

  filterBySesh(filteredCommIds, seshFilters) {
    if (seshFilters.length != 0) {
      var filtered = seshFilters.map( function(str) {
        str = str.toUpperCase()
        var filteredInner = filteredCommIds.filter( set => set[0].includes(str))
        return filteredInner
      })
      return  filtered.flat(1)
    } else {
      return  filteredCommIds
    }
  }

  filterByOrgin(filteredCommIds, originFilters) {
    if (originFilters.length != 0) {
      filteredCommIds = originFilters.map( function(str) {
        str = str.toUpperCase()
        return filteredCommIds.filter( set => set[0].startsWith(str))
      })
      return  filteredCommIds.flat(1)
    } else {
      return  filteredCommIds
    }
  }

  setSelectedIdIfAny() {
    let selectedIdsIfAny = JSON.parse(this.data.get("comidselected"))

    if (selectedIdsIfAny.length == 0) {
      this.commIdsTarget[0].selected = true
    }
    else {
      selectedIdsIfAny.forEach(idOption => {
        this.commIdsTarget.forEach(option => {
          if (option.text == idOption[1]) {
            option.selected = true
          }
        })
      })
    }
  }

}
